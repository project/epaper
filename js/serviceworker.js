self.addEventListener("fetch", event => {
  console.log('WORKER: Fetching', event.request);
});

self.addEventListener("install", event => {
  /*
   You can use the install event to pre-cache any important and long-lived files (such as CSS files,
   JavaScript files, logo images, or offline pages) from your app.
   */
  console.log("WORKER: install event in progress.");
});

self.addEventListener("activate", event => {
  /*
  Use this event to clean up outdated caches.
   */
  console.log("WORKER: activate event in progress.");
});
