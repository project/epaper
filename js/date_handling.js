(function (Drupal) {
  Drupal.behaviors.epaperDateHandling = {
    attach: function attach(context) {
      let dates = context.querySelector('#epaper-settings-form textarea#edit-dates');
      if (!dates) {
        return;
      }
      console.log(dates, "Calendar");
      let calendar = new FullCalendar.Calendar(dates, {
        initialView: 'dayGridMonth'
      });

      calendar.render();
    }
  };
})(Drupal);
