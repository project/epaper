if (
  "serviceWorker" in navigator
  && pwaPath
) {
  navigator.serviceWorker.register(pwaPath);
}
