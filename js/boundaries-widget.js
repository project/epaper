(function ($, Drupal) {

  /* global Cropper */

  Drupal.behaviors.epaperWidgetBehavior = {
    attach: function (context, settings) {
      $('.epaper-boundaries-widget-page-image img', context).once('epaper-boundaries-widget').each(function (index, image) {
        let parent = $(this).parent().parent();
        let top = parent.find("input[id$='-top']");
        let left = parent.find("input[id$='-left']");
        let width = parent.find("input[id$='-width']");
        let height = parent.find("input[id$='-height']");

        let cropperHandling = () => {
          const cropper = new Cropper(image, {
            viewMode: 2,
            ready: function () {
              cropper.setData({
                x: (parseFloat(left.val()) ?? 10) / 100 * image.naturalWidth,
                y: (parseFloat(top.val()) ?? 10) / 100 * image.naturalHeight,
                width: (parseFloat(width.val()) ?? 30) / 100 * image.naturalWidth,
                height: (parseFloat(height.val()) ?? 30) / 100 * image.naturalHeight,
              });

              image.addEventListener('crop', (event) => {
                left.val((event.detail.x / image.naturalWidth * 100).toFixed(2));
                top.val((event.detail.y / image.naturalHeight * 100).toFixed(2));
                width.val((event.detail.width / image.naturalWidth * 100).toFixed(2));
                height.val((event.detail.height / image.naturalHeight * 100).toFixed(2));
              });
            },
          });
        }

        if (image.naturalHeight !== 0) {
          cropperHandling();
        }
        else {
          image.onload = () => {
            cropperHandling();
          };
        }
      });
    }
  };

})(jQuery, Drupal);
