import { PageElementBase } from './src/PageElementBase.js'

// todo: how?
/*
document.querySelectorAll(".element-placeholder").forEach((elementPlaceholder) => {
  let element = new PageElementBase(elementPlaceholder);
  elementPlaceholder.addEventListener('click', (e) => {
    element.onClick(e);
  });
});
 */
document.querySelector("#horizontalScrollToggle").addEventListener('click', (e) => {
  e.preventDefault();
  document.querySelector('.pages-container')?.classList.toggle('horizontal-scroll');
  document.querySelector('.pages-container')?.classList.toggle('container-xl');
});

/*
 * ### Open page by URL fragment for Search ###
 */
let urlPageTarget;
let urlArticleTarget;
if (window.location.hash.length) {
  let hashParts = window.location.hash.substring(1).split('-');
  if (hashParts[0]) {
    urlPageTarget = hashParts[0];
  }
  if (hashParts[1]) {
    urlArticleTarget = hashParts[1];
  }

  let scrollTarget = document.getElementById('page-' + urlPageTarget);
  if (scrollTarget) {
    scrollTarget.scrollIntoView({
      behavior: 'smooth'
    });
  }

  if (urlArticleTarget) {
    if (link.prop('id') === 'article-' + urlArticleTarget) {
      link.trigger('click');
    }
  }
}
