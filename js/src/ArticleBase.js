import {PageElementBase} from "./PageElementBase.js";

export class ArticleBase extends PageElementBase {

  onClick (e) {
    super.onClick(e);

    if (!this.modalContainer) {
      return;
    }

    let data = JSON.parse(this.element.dataset.json);

    if (!data) {
      return;
    }
    this.articleData = data;

    this.modalContainer.querySelector('.article-title').textContent = this.articleData.title ?? this.element.dataset.name;
    this.modalContainer.querySelector('.article-author').textContent = this.articleData.author ?? '';
    this.modalContainer.querySelector('.article-preface').textContent = this.articleData.teaser ?? '';
    this.modalContainer.querySelector('.article-text').innerHTML = this.articleData.content ?? '';

    if (this.articleData.images.length) {
      let template = this.modalContainer.querySelector('template.gallery-image');
      this.articleData.images.forEach((imageData, index) => {
        let image = template.content.firstElementChild.cloneNode(true);
        image.querySelector('img').setAttribute('src', imageData.src);
        image.querySelector('p').textContent = imageData.title;
        if (index === 0) {
          image.classList.add('active');
        }
        this.modalContainer.querySelector('.carousel-inner').appendChild(image);
      });

      if (this.articleData.images.length > 1) {
        this.modalContainer.querySelector('.carousel-control-prev').classList.remove('d-none');
        this.modalContainer.querySelector('.carousel-control-next').classList.remove('d-none');
      }
      else {
        this.modalContainer.querySelector('.carousel-control-prev').classList.add('d-none');
        this.modalContainer.querySelector('.carousel-control-next').classList.add('d-none');
      }
      this.modalContainer.querySelector('.article-images').classList.remove('d-none');
      this.modalContainer.querySelector('.article-images').setAttribute('id', 'article-images');
    }

    //
    // Speech handling
    //
    let speakButton = this.modalContainer.querySelector('button.speak');
    let pauseButton = this.modalContainer.querySelector('button.pause');
    if ('speechSynthesis' in window) {
      window.speechSynthesis.cancel();
      speakButton.addEventListener('click', (e) => {
        e.preventDefault();
        let synth = window.speechSynthesis;
        if (synth.paused) {
          synth.resume();
        }
        else {
          let msg = new SpeechSynthesisUtterance();
          msg.text = '';
          msg.text += this.modalContainer.querySelector('.article-title')?.textContent;
          msg.text += this.modalContainer.querySelector('.article-author')?.textContent;
          msg.text += this.modalContainer.querySelector('.article-teaser')?.textContent;
          msg.text += this.modalContainer.querySelector('.article-text')?.textContent;
          msg.lang = 'de-DE';
          synth.speak(msg);
        }

        speakButton.classList.add("d-none");
        pauseButton.classList.remove("d-none");
      });
      pauseButton.addEventListener('click', (e) => {
        e.preventDefault();
        if ('speechSynthesis' in window) {
          window.speechSynthesis.pause();
        }
        speakButton.classList.remove("d-none");
        pauseButton.classList.add("d-none");
      });
    }
    else {
      pauseButton.classList.add("d-none");
      speakButton.classList.add("d-none");
    }

    //
    // Text / Cutout handling
    //
    let articleButton = this.modalContainer.querySelector('button.cutout-trigger');
    let textButton = this.modalContainer.querySelector('button.text-trigger');
    if (articleButton) {
      articleButton.addEventListener('click', (e) => {
        e.preventDefault();
        this.modalContainer.classList.add('cutout-shown');
        this.drawElementCanvas();
      });
    }
    if (textButton) {
      textButton.addEventListener('click', (e) => {
        e.preventDefault();
        this.modalContainer.classList.remove('cutout-shown');
      });
    }

    //
    // Font size handling
    //
    let fontButtons = this.modalContainer.querySelector('.font-size-adjust');
    fontButtons.querySelector('.font-size-decrease').addEventListener('click', () => {
      this.setArticleFontSize(this.getArticleFontSize() - 0.1);
    });
    fontButtons.querySelector('.font-size-default').addEventListener('click', () => {
      this.setArticleFontSize(1);
    });
    fontButtons.querySelector('.font-size-increase').addEventListener('click', () => {
      this.setArticleFontSize(this.getArticleFontSize() + 0.1);
    });

  }
}
