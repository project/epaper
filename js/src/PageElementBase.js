
/**
 * @property {HTMLElement} element
 * @property {Node} modal
 */
export class PageElementBase {

  modalIdentifier = '#elementModal'

  /**
   * @param {HTMLElement} element
   */
  constructor (element) {
    this.element = element;
    this.parentPage = element.closest('.page');
  }

  setArticleFontSize(fontSize = null) {

    if (!fontSize) {
      fontSize = this.getArticleFontSize();
    }

    if (fontSize > 1.5) {
      fontSize = 1.5;
    }
    else if (fontSize < 0.5) {
      fontSize = 0.5;
    }

    this.modalContainer.style.fontSize = fontSize + 'rem';
    document.cookie = "ePaperFontSize=" + fontSize + '; SameSite=Strict; max-age=0';
  }

  getArticleFontSize() {
    let epaperFontSetting = document.cookie.match(new RegExp(
      "(?:^|; )" + "ePaperFontSize" + "=([^;]*)"
    ));
    return epaperFontSetting ? parseFloat(epaperFontSetting[0]) : 1;
  }

  drawElementCanvas() {
    let image = this.parentPage.querySelector('picture img');
    if (!image) {
      return;
    }

    const canvasParent = this.modalContainer.querySelector('.cutout');
    if (!canvasParent) {
      return;
    }
    const canvas = this.modalContainer.querySelector('.cutout canvas');
    if (!canvas) {
      return;
    }

    let cutout = new Image();
    cutout.src = image.currentSrc ?? image.src;

    let sourceWidth = image.naturalWidth * (this.element.dataset.width / 100);
    let sourceHeight = image.naturalHeight * (this.element.dataset.height / 100);

    canvas.width = canvasParent.clientWidth;
    canvas.height = canvasParent.clientWidth * (sourceHeight / sourceWidth);

    if (canvas.height > canvasParent.clientHeight) {
      canvas.height = canvasParent.clientHeight;
      canvas.width = canvasParent.clientHeight * (sourceWidth / sourceHeight)
    }

    canvas.getContext('2d').drawImage(
      image,
      image.naturalWidth * (this.element.dataset.left / 100),
      image.naturalHeight * (this.element.dataset.top / 100),
      sourceWidth,
      sourceHeight,
      0,
      0,
      canvas.width,
      canvas.height
    );
  }

  onClick(e) {
    e.preventDefault();

    if (!this.modalIdentifier) {
      return;
    }

    if (!document.querySelector(this.modalIdentifier)) {
      return;
    }

    this.modalContainer = document.querySelector(this.modalIdentifier).cloneNode(true);
    if (!this.modalContainer) {
      return;
    }
    this.modalContainer.id += '-' + this.element.id + '-';
    this.modalContainer.id += Math.floor(Math.random() * 10000);

    let modalTitleElement = this.modalContainer.querySelector('.modal-title');
    if (modalTitleElement) {
      modalTitleElement.textContent = this.element.getAttribute('data-name');
      modalTitleElement.classList.remove('d-none');
    }
    this.modal = new bootstrap.Modal(this.modalContainer);
    this.modalContainer.addEventListener('hidden.bs.modal', () => {
      this.modalContainer.remove();
    });
    this.modal.show();

    this.modalContainer.addEventListener('shown.bs.modal', () => {
      this.drawElementCanvas();
    });

    if ('speechSynthesis' in window) {
      window.speechSynthesis.cancel();
    }

    this.setArticleFontSize();

    //
    // PagePreview
    //
    const pagePreviewContainer = document.querySelector("#pagePreviewContainer");
    if (
      pagePreviewContainer
      && pagePreviewContainer.classList.contains('show')
    ) {
      document.querySelector('button[data-target="#pagePreviewContainer"]').click();
    }

    //
    // Article navigation handling
    //
    let articleNavigationButtons = this.modalContainer.querySelector('.element-navigation');
    if (!articleNavigationButtons) {
      return;
    }

    let lastArticle = this.element.previousElementSibling;
    let nextArticle = this.element.nextElementSibling;

    if (lastArticle) {
      articleNavigationButtons.querySelector('.last-article').addEventListener('click', () => {
        this.modal.hide();
        setTimeout(function(){
          lastArticle.click();
        }, 200);
      });
    }
    else {
      articleNavigationButtons.querySelector('.last-article').classList.add('disabled');
    }

    if (nextArticle) {
      articleNavigationButtons.querySelector('.next-article').addEventListener('click', () => {
        this.modal.hide();
        setTimeout(function(){
          nextArticle.click();
        }, 200);
      });
    }
    else {
      articleNavigationButtons.querySelector('.next-article').classList.add('disabled');
    }

    // @see https://www.kirupa.com/html5/detecting_touch_swipe_gestures.htm
    let initialX = null;
    let initialY = null;
    this.modalContainer.addEventListener('touchstart', (e) => {
      initialX = e.touches[0].clientX;
      initialY = e.touches[0].clientY;
    });
    this.modalContainer.addEventListener('touchmove', (e) => {
      if (initialX === null || initialY === null) {
        return;
      }

      let diffX = initialX - e.touches[0].clientX;
      let diffY = initialY - e.touches[0].clientY;
      initialX = null;
      initialY = null;

      if (Math.abs(diffX) > Math.abs(diffY)) {
        if (diffX > 0) {
          // Swiped left. Next Article.
          if (nextArticle) {
            this.modal.hide();
            setTimeout(function(){
              nextArticle.click();
            }, 200);
          }
        } else {
          if (lastArticle) {
            // Swiped right. Last article.
            this.modal.hide();
            setTimeout(function(){
              lastArticle.click();
            }, 200);
          }

        }
      }
    });
  }
}
