<?php

namespace Drupal\epaper\Entity;

use Drupal;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\user\UserInterface;
use Exception;
use Imagick;

/**
 * Defines the Page entity.
 *
 * @ingroup epaper
 *
 * @ContentEntityType(
 *   id = "epaper_page",
 *   label = @Translation("Page"),
 *   bundle_label = @Translation("Page type"),
 *   handlers = {
 *     "views_data" = "Drupal\epaper\ViewsData\EpaperPageViewsData",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\epaper\Routing\EpaperPageRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\epaper\Form\EpaperPageForm",
 *       "add" = "Drupal\epaper\Form\EpaperPageForm",
 *       "edit" = "Drupal\epaper\Form\EpaperPageForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm"
 *     },
 *     "access" = "Drupal\epaper\PageAccessControlHandler",
 *   },
 *   base_table = "epaper_page",
 *   data_table = "epaper_page_field_data",
 *   revision_table = "epaper_page_revision",
 *   revision_data_table = "epaper_page_field_revision",
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   show_revision_ui = TRUE,
 *   translatable = FALSE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer page entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/epaper/pages/{epaper_page}",
 *     "add-page" = "/admin/epaper/pages/add",
 *     "add-form" = "/admin/epaper/pages/add/{epaper_page_type}",
 *     "edit-form" = "/admin/epaper/pages/{epaper_page}/edit",
 *     "delete-form" = "/admin/epaper/pages/{epaper_page}/delete",
 *     "delete-multiple-form" = "/admin/epaper/pages/delete",
 *     "version-history" = "/admin/epaper/pages/{epaper_page}/revisions",
 *     "revision" = "/admin/epaper/pages/{epaper_page}/revisions/{epaper_page_revision}/view",
 *     "revision_revert" = "/admin/epaper/pages/{epaper_page}/revisions/{epaper_page_revision}/revert",
 *     "revision_delete" = "/admin/epaper/pages/{epaper_page}/revisions/{epaper_page_revision}/delete",
 *   },
 *   bundle_entity_type = "epaper_page_type",
 *   field_ui_base_route = "entity.epaper_page_type.edit_form"
 * )
 */
class EpaperPage extends EditorialContentEntityBase implements EpaperPageInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  private FileSystemInterface $file_system;

  public static function preCreate(EntityStorageInterface $storage, array &$values): void {
    parent::preCreate($storage, $values);
    $values += [
      'user_id' => Drupal::currentUser()->id(),
    ];
  }

  protected function urlRouteParameters($rel): array {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert') {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete') {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  public function getImageUri(): ?string {
    return $this->get('image')?->entity?->getFileUri();
  }

  function getIssue(): ?EpaperIssueInterface {
    /** @var \Drupal\epaper\Entity\EpaperIssueInterface $issue */
    $issue = $this->get('issue_id')?->entity;
    if (!$issue) {
      return NULL;
    }

    return $issue;
  }

  function getNumber(): ?int {
    return $this->get('page_number')->value;
  }

  public function label(): string {
    $label = $this->getIssue()?->getName();
    if ($this->getName()) {
      $label .= ' - ' .$this->getName();
    }
    $label .= ' - ' . $this->get('page_number')->value;

    return $label;
  }

  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    // If no revision author has been set explicitly,
    // make the epaper_page owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  public function getName(): string {
    return $this->get('name')?->value ?? '';
  }

  public function setName(string $name): EpaperPageInterface {
    $this->set('name', $name);
    return $this;
  }

  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  public function setCreatedTime(int $timestamp): EpaperPageInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  public function getOwner(): UserInterface {
    return $this->get('user_id')->entity;
  }

  public function getOwnerId(): int {
    return $this->get('user_id')->target_id;
  }

  public function setOwnerId($uid): EpaperPageInterface {
    $this->set('user_id', $uid);
    return $this;
  }

  public function setOwner(UserInterface $account): EpaperPageInterface {
    $this->set('user_id', $account->id());
    return $this;
  }

  public function delete(): void {
    parent::delete();

    if (!$this->get('pdf')?->isEmpty()) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->get('pdf')?->entity;
      $this->fileSystem()->delete($file->getFileUri());
      $file->delete();
    }

    if (!$this->get('image')?->isEmpty()) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->get('image')?->entity;
      $this->fileSystem()->delete($file->getFileUri());
      $file->delete();
    }


    $element_storage = $this->entityTypeManager()->getStorage('epaper_element');
    // TODO: Filter by page
    $elements = $element_storage->loadByProperties([
      'bounds.page_id' => $this->id(),
    ]);
    $element_storage->delete($elements);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['issue_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Issue'))
      ->setRequired(TRUE)
      ->setDescription(t('Parent issue.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'epaper_issue')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Page entity.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(0)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Page entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 100,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['page_number'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Page number'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE);

    $fields['width'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Width'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE);

    $fields['height'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Height'))
      ->setRevisionable(TRUE);

    $fields['double_page'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Panorama / Double page'))
      ->setDefaultValue(FALSE)
      ->setRevisionable(TRUE)
      ->setDescription(t('A boolean indicating whether the Page is panorama.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ]);

    $fields['status']->setDescription(t('A boolean indicating whether the Page is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Image'))
      ->setRequired(TRUE)
      ->setDescription(t('Image field'))
      ->setSettings([
        'file_directory' => 'publications/temp/pages/[current-date:html_date]',
        'alt_field_required' => FALSE,
        'file_extensions' => 'png jpg jpeg',
      ])
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'image_url',
      ))
      ->setDisplayOptions('form', array(
        'label' => 'hidden',
        'type' => 'image_image',
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['pdf'] = BaseFieldDefinition::create('file')
      ->setLabel('PDF')
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => 'publications/temp/pages/[current-date:html_date]',
        'file_extensions' => 'pdf',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);

    if (!$this->get('pdf')->isEmpty()) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->get('pdf')?->entity;
      if ($file) {
        $destination_uri = $this->getDirectory() . '/' . basename($file->getFileUri());
        if ($file->getFileUri() !== $destination_uri) {
          $this->fileSystem()->move($file->getFileUri(), $destination_uri);

          if (is_file($destination_uri)) {
            $file->setFileUri($destination_uri);
            $file->save();
          }
        }
      }
    }

    if (!$this->get('image')->isEmpty()) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->get('image')?->entity;
      if ($file) {
        $destination_uri = $this->getDirectory() . '/' . basename($file->getFileUri());
        if ($file->getFileUri() !== $destination_uri) {
          $this->fileSystem()->move($file->getFileUri(), $destination_uri);

          if (is_file($destination_uri)) {
            $file->setFileUri($destination_uri);
            $file->save();
          }
        }
      }
    }
  }

  public function getElements(): array {
    /** @var \Drupal\epaper\Entity\EpaperElementInterface[] $elements */
    $elements = $this->entityTypeManager()->getStorage('epaper_element')->loadByProperties([
      'bounds.page_id' => $this->id(),
    ]);

    return $elements;
  }

  public function getDirectory($realpath = FALSE): string {
    $directory = $this->getIssue()->getDirectory() . '/pages';

    if (!is_dir($directory)) {
      $this->fileSystem()->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY|FileSystemInterface::MODIFY_PERMISSIONS);
    }

    if ($realpath) {
      return $this->fileSystem()->realpath($directory);
    }

    return $directory;
  }

  protected function fileSystem(): FileSystemInterface {
    if (!isset($this->file_system)) {
      $this->file_system = Drupal::service('file_system');
    }

    return $this->file_system;
  }

  public function setPdfByImagick(Imagick $imagick): bool {
    $page_pdf_field = $this->get('pdf');
    if (!$page_pdf_field->isEmpty()) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $page_pdf_field->entity;
      if ($file) {
        if (is_file($file->getFileUri())) {
          $this->fileSystem()->delete($file->getFileUri());
        }
        $file->delete();
      }
      $page_pdf_field->removeItem(0);
    }

    $imagick->setImageFormat("pdf");
    $destination = $this->getDirectory() . '/' . $this->getNumber() . '.pdf';
    $this->fileSystem()->delete($destination);
    if ($imagick->writeImage($this->getDirectory(TRUE) . '/' . $this->getNumber() . '.pdf')) {
      if (!is_file($destination)) {
        throw new Exception(t('File %file should be written, but was not found', ['%file' => $destination]));
      }
      $file = File::create([
        'uri' => $destination,
        'status' => 1,
      ]);
      $file->save();
      $page_pdf_field->appendItem($file->id());
    }

    return TRUE;
  }

  public function setImageByImagick(Imagick $imagick): bool {
    $page_image_field = $this->get('image');
    if (!$page_image_field->isEmpty()) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $page_image_field->entity;
      if ($file) {
        if (is_file($file->getFileUri())) {
          $this->fileSystem()->delete($file->getFileUri());
        }
        $file->delete();
      }
      $page_image_field->removeItem(0);
    }
    $imagick->setImageBackgroundColor('#ffffff');
    $imagick = $imagick->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
    $imagick->setImageCompressionQuality(100);
    $imagick->setImageFormat("jpeg");
    $destination = $this->getDirectory() . '/' . $this->getNumber() . '.jpg';
    $this->fileSystem()->delete($destination);
    if ($imagick->writeImage($this->getDirectory(TRUE) . '/' . $this->getNumber() . '.jpg')) {
      if (!is_file($destination)) {
        throw new Exception(t('File %file should be written, but was not found', ['%file' => $destination]));
      }
      $file = File::create([
        'uri' => $destination,
        'status' => 1,
      ]);
      $file->save();
      $page_image_field->appendItem($file->id());
    }

    return TRUE;
  }

}
