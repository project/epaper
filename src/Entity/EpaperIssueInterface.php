<?php

namespace Drupal\epaper\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Url;

/**
 * Provides an interface for defining Epaper entities.
 *
 * @ingroup epaper
 */
interface EpaperIssueInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface {

  public function getName(): string;

  public function getDirectory(): ?string;

  public function setName(string $name): EpaperIssueInterface;

  /**
   * @return \Drupal\epaper\Entity\EpaperPageInterface[]
   */
  public function getPages(): array;

  public function getPublication(): ?EpaperPublicationInterface;

  public function getRenderedUrl(): string;

  public function pdf2Page(int $page_number): ?EpaperPageInterface;

}
