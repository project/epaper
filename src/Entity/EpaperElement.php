<?php

namespace Drupal\epaper\Entity;

use Drupal;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Element entity.
 *
 * @ingroup epaper
 *
 * @ContentEntityType(
 *   id = "epaper_element",
 *   label = @Translation("Element"),
 *   bundle_label = @Translation("Element type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\epaper\Routing\EpaperElementRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\epaper\Form\ElementForm",
 *       "add" = "Drupal\epaper\Form\ElementForm",
 *       "edit" = "Drupal\epaper\Form\ElementForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm"
 *     },
 *     "access" = "Drupal\epaper\ElementAccessControlHandler",
 *   },
 *   base_table = "epaper_element",
 *   data_table = "epaper_element_field_data",
 *   revision_table = "epaper_element_revision",
 *   revision_data_table = "epaper_element_field_revision",
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   translatable = FALSE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer element entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/epaper/elements/{epaper_element}",
 *     "add-page" = "/admin/epaper/elements/add",
 *     "add-form" = "/admin/epaper/elements/add/{epaper_element_type}",
 *     "edit-form" = "/admin/epaper/elements/{epaper_element}/edit",
 *     "delete-form" = "/admin/epaper/elements/{epaper_element}/delete",
 *     "version-history" = "/admin/epaper/elements/{epaper_element}/revisions",
 *     "revision" = "/admin/epaper/elements/{epaper_element}/revisions/{epaper_element_revision}/view",
 *     "revision_revert" = "/admin/epaper/elements/{epaper_element}/revisions/{epaper_element_revision}/revert",
 *     "revision_delete" = "/admin/epaper/elements/{epaper_element}/revisions/{epaper_element_revision}/delete",
 *   },
 *   bundle_entity_type = "epaper_element_type",
 *   field_ui_base_route = "entity.epaper_element_type.edit_form"
 * )
 */
class EpaperElement extends EditorialContentEntityBase implements EpaperElementInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values): void {
    parent::preCreate($storage, $values);
    $values += [
      'user_id' => Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert') {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete') {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    // If no revision author has been set explicitly,
    // make the epaper_element owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->get('name')->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name): EpaperElement {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): EpaperElement {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): int {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): EpaperElement {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): EpaperElement {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['bounds'] = BaseFieldDefinition::create('epaper_bounds')
      ->setLabel(t('Boundaries'))
      ->setRevisionable(TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Element entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the Element entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Element is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  public function getPage(): ?EpaperPageInterface {
    $page_id = $this->get('bounds')?->page_id;
    if (!$page_id) {
      return NULL;
    }

    /** @var \Drupal\epaper\Entity\EpaperPageInterface $page */
    $page = Drupal::entityTypeManager()->getStorage('epaper_page')->load($page_id);

    return $page;
  }

  public function getEpaper(): ?EpaperIssueInterface {
    /** @var \Drupal\epaper\Entity\DefaultEpaperPage $page */
    $page = $this->getPage();
    if (!$page) {
      return NULL;
    }

    /** @var \Drupal\epaper\Entity\EpaperIssueInterface $epaper */
    $epaper = $page->get('issue_id')?->entity;
    if (!$epaper) {
      return NULL;
    }

    return $epaper;
  }
}
