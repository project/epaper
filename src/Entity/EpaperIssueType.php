<?php

namespace Drupal\epaper\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Epaper type entity.
 *
 * @ConfigEntityType(
 *   id = "epaper_issue_type",
 *   label = @Translation("Issue type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\epaper\GenericConfigListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "add" = "Drupal\epaper\Form\EpaperIssueTypeForm",
 *       "edit" = "Drupal\epaper\Form\EpaperIssueTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "epaper_issue_type",
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   admin_permission = "administer site configuration",
 *   bundle_of = "epaper_issue",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/epaper_issue_type/{epaper_issue_type}",
 *     "add-form" = "/admin/structure/epaper_issue_type/add",
 *     "edit-form" = "/admin/structure/epaper_issue_type/{epaper_issue_type}/edit",
 *     "delete-form" = "/admin/structure/epaper_issue_type/{epaper_issue_type}/delete",
 *     "collection" = "/admin/structure/epaper_issue_type"
 *   }
 * )
 */
class EpaperIssueType extends ConfigEntityBundleBase {

  /**
   * The Epaper type ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The Epaper type label.
   *
   * @var string
   */
  protected string $label;

}
