<?php

namespace Drupal\epaper\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Publication type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "epaper_publication_type",
 *   label = @Translation("Publication type"),
 *   label_collection = @Translation("Publication types"),
 *   label_singular = @Translation("Publication type"),
 *   label_plural = @Translation("Publications types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count publications type",
 *     plural = "@count publications types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\epaper\Form\EpaperPublicationTypeForm",
 *       "edit" = "Drupal\epaper\Form\EpaperPublicationTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\epaper\GenericConfigListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer publication types",
 *   bundle_of = "epaper_publication",
 *   config_prefix = "epaper_publication_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/epaper_publication_types/add",
 *     "edit-form" = "/admin/structure/epaper_publication_types/manage/{epaper_publication_type}",
 *     "delete-form" = "/admin/structure/epaper_publication_types/manage/{epaper_publication_type}/delete",
 *     "collection" = "/admin/structure/epaper_publication_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class EpaperPublicationType extends ConfigEntityBundleBase {

  /**
   * The machine name of this publication type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the publication type.
   *
   * @var string
   */
  protected $label;

}
