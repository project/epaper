<?php

namespace Drupal\epaper\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface for defining Page entities.
 *
 * @ingroup epaper
 */
interface EpaperPageInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  public function getImageUri(): ?string;

  public function getName(): string;

  public function setName(string $name): EpaperPageInterface;

  public function getCreatedTime(): int;

  public function setCreatedTime(int $timestamp): EpaperPageInterface;

  public function getRevisionCreationTime();

  public function setRevisionCreationTime($timestamp);

  public function getRevisionUser();

  public function setRevisionUserId($user_id);

  public function getIssue(): ?EpaperIssueInterface;

  public function getNumber(): ?int;

}
