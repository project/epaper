<?php

namespace Drupal\epaper\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Page type entity.
 *
 * @ConfigEntityType(
 *   id = "epaper_page_type",
 *   label = @Translation("Page type"),
 *   label_collection = @Translation("Page types"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\epaper\GenericConfigListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "add" = "Drupal\epaper\Form\EpaperPageTypeForm",
 *       "edit" = "Drupal\epaper\Form\EpaperPageTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "epaper_page_type",
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   admin_permission = "administer site configuration",
 *   bundle_of = "epaper_page",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/epaper_page_type/{epaper_page_type}",
 *     "add-form" = "/admin/structure/epaper_page_type/add",
 *     "edit-form" = "/admin/structure/epaper_page_type/{epaper_page_type}/edit",
 *     "delete-form" = "/admin/structure/epaper_page_type/{epaper_page_type}/delete",
 *     "collection" = "/admin/structure/epaper_page_type"
 *   }
 * )
 */
class EpaperPageType extends ConfigEntityBundleBase {

  protected string $id;

  protected string $label;

}
