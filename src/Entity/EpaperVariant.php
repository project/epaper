<?php

namespace Drupal\epaper\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the epaper variant entity class.
 *
 * @ContentEntityType(
 *   id = "epaper_variant",
 *   label = @Translation("Epaper Variant"),
 *   label_collection = @Translation("Epaper Variants"),
 *   label_singular = @Translation("epaper variant"),
 *   label_plural = @Translation("epaper variants"),
 *   label_count = @PluralTranslation(
 *     singular = "@count epaper variants",
 *     plural = "@count epaper variants",
 *   ),
 *   bundle_label = @Translation("Epaper Variant type"),
 *   handlers = {
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\epaper\Form\EpaperVariantForm",
 *       "edit" = "Drupal\epaper\Form\EpaperVariantForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\epaper\Routing\EpaperVariantRouteProvider",
 *     }
 *   },
 *   base_table = "epaper_variant",
 *   admin_permission = "administer epaper variant types",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "bundle",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/epaper/variant/add/{epaper_variant_type}",
 *     "add-page" = "/admin/epaper/variant/add",
 *     "canonical" = "/admin/epaper/variant/{epaper_variant}",
 *     "edit-form" = "/admin/epaper/variant/{epaper_variant}",
 *     "delete-form" = "/admin/epaper/variant/{epaper_variant}/delete",
 *   },
 *   bundle_entity_type = "epaper_variant_type",
 *   field_ui_base_route = "entity.epaper_variant_type.edit_form",
 * )
 */
class EpaperVariant extends ContentEntityBase implements EpaperVariantInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the epaper variant was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the epaper variant was last edited.'));

    $fields['logo_override'] = BaseFieldDefinition::create('file')
      ->setLabel('Logo Override')
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => '[epaper_variant:directory]',
        'file_extensions' => 'png gif jpg jpeg',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
