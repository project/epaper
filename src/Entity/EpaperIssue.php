<?php

namespace Drupal\epaper\Entity;

use Drupal;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Exception;
use Imagick;

/**
 * Defines the Epaper entity.
 *
 * @ingroup epaper
 *
 * @ContentEntityType(
 *   id = "epaper_issue",
 *   label = @Translation("Issue"),
 *   label_collection = @Translation("Issues"),
 *   label_singular = @Translation("Issue"),
 *   label_plural = @Translation("issues"),
 *   label_count = @PluralTranslation(
 *     singular = "@count issue",
 *     plural = "@count issues",
 *   ),
 *   bundle_label = @Translation("Issue type"),
 *   handlers = {
 *     "views_data" = "Drupal\epaper\ViewsData\EpaperIssueViewsData",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "access" = "Drupal\epaper\EpaperIssueAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\epaper\Form\EpaperIssueForm",
 *       "add" = "Drupal\epaper\Form\EpaperIssueForm",
 *       "edit" = "Drupal\epaper\Form\EpaperIssueForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\epaper\Routing\EpaperIssueRouteProvider",
 *     },
 *   },
 *   base_table = "epaper_issue",
 *   data_table = "epaper_issue_field_data",
 *   revision_table = "epaper_issue_revision",
 *   revision_data_table = "epaper_issue_field_revision",
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   show_revision_ui = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer epaper issue entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/epaper/issue/{epaper_issue}",
 *     "wizard" = "/admin/epaper/issue/wizard",
 *     "add-page" = "/admin/epaper/issue/add",
 *     "add-form" = "/admin/epaper/issue/add/{epaper_issue_type}",
 *     "edit-form" = "/admin/epaper/issue/{epaper_issue}/edit",
 *     "delete-form" = "/admin/epaper/issue/{epaper_issue}/delete",
 *     "delete-multiple-form" = "/admin/epaper/issue/delete",
 *     "version-history" = "/admin/epaper/issue/{epaper_issue}/revisions",
 *     "revision" = "/admin/epaper/issue/{epaper_issue}/revisions/{epaper_revision}/view",
 *     "revision_revert" = "/admin/epaper/issue/{epaper_issue}/revisions/{epaper_revision}/revert",
 *     "revision_delete" = "/admin/epaper/issue/{epaper_issue}/revisions/{epaper_revision}/delete",
 *     "rendered" = "/epaper/{epaper_issue}",
 *   },
 *   bundle_entity_type = "epaper_issue_type",
 *   field_ui_base_route = "entity.epaper_issue_type.edit_form"
 * )
 */
class EpaperIssue extends EditorialContentEntityBase implements EpaperIssueInterface {

  private string $directory = '';
  private FileSystemInterface $file_system;

  use EntityChangedTrait;
  use EntityPublishedTrait;
  use StringTranslationTrait;

  function getPublication(): ?EpaperPublicationInterface {
    /** @var \Drupal\epaper\Entity\EpaperPublicationInterface $publication */
    $publication = $this->get('publication_id')?->entity;
    if (!$publication) {
      return NULL;
    }

    return $publication;
  }

  public function label(): string {
    $label = $this->getPublication()?->label();
    $label .= ' - ' . $this->getName();

    return $label;
  }

  protected function urlRouteParameters($rel): array {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert') {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete') {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  public function getName(): string {
    return $this->get('name')?->value ?? '';
  }

  public function setName(string $name): EpaperIssueInterface {
    $this->set('name', $name);
    return $this;
  }

  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  public function setCreatedTime($timestamp): self {
    $this->set('created', $timestamp);
    return $this;
  }

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['publication_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Publication'))
      ->setRequired(TRUE)
      ->setDescription(t('The Publication ID of this Issue entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'epaper_publication')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['pages'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Pages'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Epaper entity.'))
      ->setSettings([
        'max_length' => 128,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['issue_date'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Issue Date'))
      ->setDescription(t('Date.'));

    $fields['status']->setDescription(t('A boolean indicating whether the Page is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['variant'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Variant'))
      ->setCardinality(1)
      ->setDescription(t('(Optional) Regional or other variants to this publication variant of this issue.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'epaper_variant')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['full_pdf'] = BaseFieldDefinition::create('file')
      ->setLabel('Full PDF')
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => 'publications/temp/issues/[current-date:html_date]',
        'file_extensions' => 'pdf',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);

    if (!$this->get('full_pdf')->isEmpty()) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->get('full_pdf')?->entity;
      if ($file) {
        $destination_uri = $this->getDirectory() . '/' . basename($file->getFileUri());
        if ($file->getFileUri() !== $destination_uri) {
          $this->fileSystem()->move($file->getFileUri(), $destination_uri);

          if (is_file($destination_uri)) {
            $file->setFileUri($destination_uri);
            $file->save();
          }
        }
      }
    }
  }

  public function delete(): void {
    parent::delete();

    if (!$this->get('full_pdf')?->isEmpty()) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->get('full_pdf')?->entity;
      $this->fileSystem()->delete($file->getFileUri());
      $file->delete();
    }

    $page_storage = $this->entityTypeManager()->getStorage('epaper_page');
    $pages = $page_storage->loadByProperties([
      'issue_id' => $this->id(),
    ]);
    $page_storage->delete($pages);
  }

  public function getPages(): array {
    /** @var \Drupal\epaper\Entity\EpaperPageInterface[] $pages */
    $pages = $this->entityTypeManager()->getStorage('epaper_page')->loadByProperties([
      'issue_id' => $this->id(),
    ]);

    usort($pages, fn ($a, $b) => $a->page_number->value / $b->page_number->value);

    return $pages;
  }

  public function getRenderedUrl(): string {
    $publication = $this->getPublication();
    if (!$publication) {
      throw new Exception('Issue has no associated publication.');
    }

    $request = Drupal::request();

    if (!$request->getHost()) {
      throw new Exception('No request found.');
    }

    $domain_parts = explode('.', $request->getHost());
    if (!$domain_parts) {
      throw new Exception('No domain found.');
    }

    $host = NULL;
    if (count($domain_parts) === 1) {
      $host = $publication->get('slug')->value . '.' . array_pop($domain_parts);
    }
    else {
      $subdomain = array_shift($domain_parts);
      if ($subdomain === $publication->get('slug')->value) {
        $host = $request->getHost();
      }
      else {
        $host = $publication->get('slug')->value . '.' . $request->getHost();
      }
    }

    return Drupal::request()->getScheme() . '://' . $host . '/app/e/' . date('Y-m-d', $this->get('issue_date')->value);
  }

  public static function directoryNameByString(string $string): string {
    // Remove any characters that are not allowed in directory names
    $cleanString = preg_replace('/[^\pL\d]+/u', '_', $string);

    // Replace consecutive underscores with a single underscore
    $cleanString = preg_replace('/_+/', '_', $cleanString);

    // Remove any leading or trailing underscores
    $cleanString = trim($cleanString, '_');

    // Convert the string to lowercase
    return strtolower($cleanString);
  }

  public function getDirectory(): ?string {
    if (!$this->directory) {
      $this->directory = 'public://publications/' . $this->publication_id->entity->id() . '-' . $this::directoryNameByString($this->publication_id->entity->label()) . '/issues/' . $this->id() . '-' . $this::directoryNameByString($this->getName());
    }

    if (!is_dir($this->directory)) {
      $this->fileSystem()->prepareDirectory($this->directory, FileSystemInterface::CREATE_DIRECTORY|FileSystemInterface::MODIFY_PERMISSIONS);
    }

    return $this->directory;
  }

  public function pdf2Page(int $page_number): ?EpaperPageInterface {
    $page_index = $page_number - 1;

    /** @var \Drupal\file\Plugin\Field\FieldType\FileItem $pdf_file_item */
    $pdf_file_item = $this->get('full_pdf')->first();

    $pdf_file_path = $pdf_file_item->entity->getFileUri();

    if (!is_file($pdf_file_path)) {
      return NULL;
    }

    $imagick = new Imagick();
    $imagick->setResolution(144, 144);
    if (!($imagick->readImage(Drupal::service('file_system')->realpath($pdf_file_path) . '[' . $page_index . ']'))) {
      throw new Exception("Could not read PDF " . $pdf_file_path);
    }
    $imagick->setImageResolution(144, 144);

    $page_storage = Drupal::entityTypeManager()->getStorage('epaper_page');
    $page_load = $page_storage->loadByProperties([
      'issue_id' => $this->id(),
      'page_number' => $page_number,
    ]);

    if ($page_load) {
      $page = array_pop($page_load);
    }
    else {
      /** @var \Drupal\epaper\Entity\EpaperPageInterface $page */
      $page = $page_storage->create([
        'issue_id' => $this->id(),
        'page_number' => $page_number,
        'name' => $this->t('Page @page', ['@page' => $page_number]),
        'double_page' => $imagick->getImageWidth() > $imagick->getImageHeight(),
        'type' => $this->publication_id->entity->get('default_page_type')->first()->getValue()['target_id'],
      ]);
    }

    $page->setPdfByImagick($imagick);

    $page->setImageByImagick($imagick);

    $page->setNewRevision();
    $page->setRevisionLogMessage('Creating / updating by batch import');

    $page->save();

    return $page;
  }

  protected function fileSystem(): FileSystemInterface {
    if (!isset($this->file_system)) {
      $this->file_system = Drupal::service('file_system');
    }

    return $this->file_system;
  }

}
