<?php

namespace Drupal\epaper\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an epaper variant entity type.
 */
interface EpaperVariantInterface extends ContentEntityInterface, EntityChangedInterface {

}
