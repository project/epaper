<?php

namespace Drupal\epaper\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the publication entity class.
 *
 * @ingroup epaper
 *
 * @ContentEntityType(
 *   id = "epaper_publication",
 *   label = @Translation("Publication"),
 *   label_collection = @Translation("Publications"),
 *   label_singular = @Translation("publication"),
 *   label_plural = @Translation("publications"),
 *   label_count = @PluralTranslation(
 *     singular = "@count publication",
 *     plural = "@count publications",
 *   ),
 *   bundle_label = @Translation("Publication type"),
 *   handlers = {
 *     "views_data" = "Drupal\epaper\ViewsData\EpaperPublicationViewsData",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "access" = "Drupal\epaper\EpaperPublicationAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\epaper\Form\EpaperPublicationForm",
 *       "edit" = "Drupal\epaper\Form\EpaperPublicationForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\epaper\Routing\EpaperPublicationRouteProvider",
 *     }
 *   },
 *   base_table = "epaper_publication",
 *   admin_permission = "administer epaper publication types",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "bundle",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "add-form" = "/admin/epaper/publication/add/{epaper_publication_type}",
 *     "add-page" = "/admin/epaper/publication/add",
 *     "canonical" = "/admin/epaper/publication/{epaper_publication}",
 *     "edit-form" = "/admin/epaper/publication/{epaper_publication}/edit",
 *     "delete-form" = "/admin/epaper/publication/{epaper_publication}/delete",
 *     "delete-multiple-form" = "/admin/epaper/publication/delete",
 *   },
 *   bundle_entity_type = "epaper_publication_type",
 *   field_ui_base_route = "entity.epaper_publication_type.edit_form",
 * )
 */
class EpaperPublication extends ContentEntityBase implements EpaperPublicationInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  protected FileSystemInterface $fileSystem;

  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  protected function storeLogo(): void {
    if (!isset($this->fileSystem)) {
      $this->fileSystem = \Drupal::service('file_system');
    }

    $currentFileField = $this->get('logo');
    if ($currentFileField->isEmpty()) {
      return;
    }

    /** @var \Drupal\file\FileInterface $currentFile */
    $currentFile = $currentFileField->entity;

    $target_directory = 'public://publications/' . $this->id() . '/logo';
    $this->fileSystem->prepareDirectory($target_directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $target_path = $target_directory . '/' . $currentFile->getFilename();

    if ($currentFile->getFileUri() == $target_path) {
      return;
    }

    $this->fileSystem->move($currentFile->getFileUri(), $target_path, FileSystemInterface::EXISTS_REPLACE);
    $currentFile->setFileUri($target_path);
    $currentFile->save();
  }

  public function getLogoUri(): ?string {
    return $this->get('logo')?->entity?->getFileUri();
  }

  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);

    $this->storeLogo();
  }

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['variants'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Variants'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('(Optional) Regional or other variants to this publication.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'epaper_variant')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['default_issue_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Default issue type'))
      ->setCardinality(1)
      ->setDescription(t('Set default issue type.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('standard')
      ->setSetting('target_type', 'epaper_issue_type')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['default_page_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Default page type'))
      ->setCardinality(1)
      ->setDescription(t('Set default page type.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('default_page')
      ->setSetting('target_type', 'epaper_page_type')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the publication was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the publication was last edited.'));

    $fields['slug'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Slug'))
      ->addConstraint('UniqueField')
      ->setDescription(t('The first part in [SUBDOMAIN].%url.', ['%url' => $host = \Drupal::request()->getHost()]))
      ->setRevisionable(TRUE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['calendar'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Calendar'))
      ->setDescription(t('Offer a calendar selection widget, if more than one issue exists.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['webapp'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Web App'))
      ->setDescription(t('Offer installation as a web app.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['logo'] = BaseFieldDefinition::create('file')
      ->setLabel('Logo')
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => 'publications/logo',
        'file_extensions' => 'png gif jpg jpeg',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  public function delete(): void {
    parent::delete();

    $issue_storage = $this->entityTypeManager()->getStorage('epaper_issue');

    $issues = $issue_storage->loadByProperties([
      'publication_id' => $this->id(),
    ]);
    $issue_storage->delete($issues);
  }

}
