<?php

namespace Drupal\epaper\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Element type entity.
 *
 * @ConfigEntityType(
 *   id = "epaper_element_type",
 *   label = @Translation("Element type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\epaper\GenericConfigListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "add" = "Drupal\epaper\Form\ElementTypeForm",
 *       "edit" = "Drupal\epaper\Form\ElementTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "epaper_element_type",
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   admin_permission = "administer site configuration",
 *   bundle_of = "epaper_element",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/epaper_element_type/{epaper_element_type}",
 *     "add-form" = "/admin/structure/epaper_element_type/add",
 *     "edit-form" = "/admin/structure/epaper_element_type/{epaper_element_type}/edit",
 *     "delete-form" = "/admin/structure/epaper_element_type/{epaper_element_type}/delete",
 *     "collection" = "/admin/structure/epaper_element_type"
 *   }
 * )
 */
class EpaperElementType extends ConfigEntityBundleBase {

  /**
   * The Element type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Element type label.
   *
   * @var string
   */
  protected $label;

}
