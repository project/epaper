<?php

namespace Drupal\epaper\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Element entities.
 *
 * @ingroup epaper
 */
interface EpaperElementInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Element name.
   *
   * @return string
   *   Name of the Element.
   */
  public function getName();

  /**
   * Sets the Element name.
   *
   * @param string $name
   *   The Element name.
   *
   * @return \Drupal\epaper\Entity\EpaperElementInterface
   *   The called Element entity.
   */
  public function setName($name);

  /**
   * Gets the Element creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Element.
   */
  public function getCreatedTime();

  /**
   * Sets the Element creation timestamp.
   *
   * @param int $timestamp
   *   The Element creation timestamp.
   *
   * @return \Drupal\epaper\Entity\EpaperElementInterface
   *   The called Element entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Element revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Element revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\epaper\Entity\EpaperElementInterface
   *   The called Element entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Element revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Element revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\epaper\Entity\EpaperElementInterface
   *   The called Element entity.
   */
  public function setRevisionUserId($uid);

}
