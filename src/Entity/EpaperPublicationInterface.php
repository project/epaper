<?php

namespace Drupal\epaper\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a publication entity type.
 */
interface EpaperPublicationInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  public function getLogoUri(): ?string;

}
