<?php

namespace Drupal\epaper\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Epaper Variant type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "epaper_variant_type",
 *   label = @Translation("Epaper Variant type"),
 *   label_collection = @Translation("Epaper Variant types"),
 *   label_singular = @Translation("epaper variant type"),
 *   label_plural = @Translation("epaper variants types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count epaper variants type",
 *     plural = "@count epaper variants types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\epaper\Form\EpaperVariantTypeForm",
 *       "edit" = "Drupal\epaper\Form\EpaperVariantTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\epaper\EpaperVariantTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer epaper variant types",
 *   bundle_of = "epaper_variant",
 *   config_prefix = "epaper_variant_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/epaper_variant_types/add",
 *     "edit-form" = "/admin/structure/epaper_variant_types/manage/{epaper_variant_type}",
 *     "delete-form" = "/admin/structure/epaper_variant_types/manage/{epaper_variant_type}/delete",
 *     "collection" = "/admin/structure/epaper_variant_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class EpaperVariantType extends ConfigEntityBundleBase {

  /**
   * The machine name of this epaper variant type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the epaper variant type.
   *
   * @var string
   */
  protected $label;

}
