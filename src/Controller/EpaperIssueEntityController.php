<?php

namespace Drupal\epaper\Controller;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\epaper\Entity\EpaperIssueInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class EpaperIssueEntityController extends BaseEntityController {

  public function renderedTitle(EpaperIssueInterface $epaper_issue): string {
    return $epaper_issue->label();
  }

  public function renderEpaperIssue(EpaperIssueInterface $epaper_issue): Response {
    $build = [
      '#theme' => 'epaper_rendered_issue',
      '#issue' => $epaper_issue,
    ];

    $renderedOutput = \Drupal::service('renderer')->render($build);

    return new Response($renderedOutput);
  }

  public function revisionOverview(EntityInterface $epaper_issue): array {
    return parent::revisionOverview($epaper_issue);
  }

  public function revisionPageTitle(EntityInterface $epaper_issue): string {
    return parent::revisionPageTitle($epaper_issue);
  }

  public function revisionShow(EntityInterface $epaper_issue): array {
    return parent::revisionShow($epaper_issue);
  }

  public function removeUnusedFiles(): Response {
    $file_storage = \Drupal::entityTypeManager()->getStorage('file');

    $query = \Drupal::database()
      ->select('file_usage', 'fu')
      ->fields('fu', ['fid']);
    $query->addJoin('LEFT OUTER', 'epaper_issue', 'ei', '[fu].id = [ei].id');
    $query
      ->condition('fu.type', 'epaper_issue')
      ->condition('ei.id', NULL, 'IS NULL');
    $fids = $query->execute()->fetchAllAssoc('fid');

    foreach ($fids as $fid => $item) {
      $file_storage->load($fid)->delete();
    }

    \Drupal::messenger()->addStatus(t('Deleted %count unused Issue files.', ['%count' => count($fids)]));

    // TODO: Remove orphaned file_managed entries and files beginning with public://publications/
    $query = \Drupal::database()
      ->select('file_managed', 'fm')
      ->fields('fm');
    $query->addJoin('LEFT OUTER', 'file_usage', 'fu', '[fm].fid = [fu].fid');
    $query
      ->condition('fm.uri', 'public://publications/%', 'LIKE')
      ->condition('fu.fid', NULL, 'IS NULL');
    $fids = $query->execute()->fetchAllAssoc('fid');

    foreach ($fids as $fid => $item) {
      $file_storage->load($fid)->delete();
    }

    \Drupal::messenger()->addStatus(t('Deleted %count orphaned Issue files.', ['%count' => count($fids)]));


    $iterator = new RecursiveIteratorIterator(
      new RecursiveDirectoryIterator(\Drupal::service('file_system')->realpath('public://publications/'), RecursiveDirectoryIterator::SKIP_DOTS),
      RecursiveIteratorIterator::CHILD_FIRST
    );

    $empty_directory_counter = 0;
    foreach ($iterator as $path) {
      if (!$path->isDir()) {
        continue;
      }
      if (iterator_count(new RecursiveDirectoryIterator($path->getRealPath(), RecursiveDirectoryIterator::SKIP_DOTS)) === 0) {
        rmdir($path->getRealPath());
        $empty_directory_counter++;
      }
    }

    \Drupal::messenger()->addStatus(t('Deleted %count empty directories below public://publications.', ['%count' => $empty_directory_counter]));

    $url = Url::fromRoute('view.epaper_issues.page_1');
    return new RedirectResponse($url->toString());
  }

}
