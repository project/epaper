<?php

namespace Drupal\epaper\Controller;

use Drupal;
use Drupal\Core\Url;
use Drupal\epaper\Entity\EpaperIssueInterface;
use Drupal\epaper\Entity\EpaperPublicationInterface;
use Exception;
use stdClass;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EpaperPublicationEntityController extends BaseEntityController {

  public function issueTitle(string $date): string {
    try {
      $issue = $this->getIssueByDateAndRequest($date);
    }
    catch (Exception) {
      return t('Unknown Issue');
    }

    if (!$issue) {
      return t('Unknown issue');
    }

    return $issue->label();
  }

  public function issue(string $date): Response {
    global $base_path;

    try {
      $issue = $this->getIssueByDateAndRequest($date);
    }
    catch (Exception $e) {
      $response = new JsonResponse();
      $response->setStatusCode(500, $this->t($e->getMessage()));
      return $response;
    }

    if (!$issue) {
      $response = new JsonResponse();
      $response->setStatusCode(404, 'Issue not found.');
      return $response;
    }

    $build = [
      '#theme' => 'epaper_rendered_issue',
      '#issue' => $issue,
    ];
    $renderedIssue = Drupal::service('renderer')->render($build);

    $publication = $issue->getPublication();

    $build = [
      '#theme' => 'epaper_app_container',
      '#content' => $renderedIssue,
      '#title' => $publication->label(),
      '#language' => Drupal::service('language_manager')->getCurrentLanguage()->getId(),
      '#theme_color' => '#2f3d58',
      '#module_base_path' => $base_path . Drupal::service('extension.list.module')->getPath('epaper'),
      '#pwa' => $publication->get('webapp')->value,
      '#calendar' => $publication->get('calendar')->value,
    ];

    if (!$publication->get('logo')->isEmpty()) {
      $icon_uri = $publication->get('logo')->entity->getFileUri();
      /** @var \Drupal\image\ImageStyleInterface $imageStyle */
      $imageStyle = \Drupal::entityTypeManager()->getStorage('image_style')->load('epaper_pwa_icon_512');
      $icon_derivative_uri = $imageStyle->buildUri($icon_uri);
      if (!is_file($icon_derivative_uri)) {
        $imageStyle->createDerivative($icon_uri, $icon_derivative_uri);
      }

      $build['#logo'] = $imageStyle->buildUrl($icon_derivative_uri);
    }

    $renderedOutput = Drupal::service('renderer')->render($build);

    return new Response($renderedOutput);
  }

  protected function getIssueByDateAndRequest(string $date, Request $request = NULL): ?EpaperIssueInterface {
    if (!$request) {
      $request = Drupal::request();
    }

    $publication = $this->getPublicationByRequest($request);

    if (!$publication) {
      throw new Exception('No publication found.');
    }

    $startTimestamp = strtotime($date . ' 00:00:00');
    $endTimestamp = strtotime($date . ' 23:59:59');

    if (!$startTimestamp || !$endTimestamp) {
      throw new Exception('Date not derivable.');
    }

    $query = Drupal::entityQuery('epaper_issue')
      ->accessCheck()
      ->condition('publication_id', $publication->id())
      ->condition('issue_date', $startTimestamp, '>=')
      ->condition('issue_date', $endTimestamp, '<=');

    $issue_ids = $query->execute();

    if (!$issue_ids) {
      return NULL;
    }

    /** @var EpaperIssueInterface $issue */
    $issue = Drupal::entityTypeManager()->getStorage('epaper_issue')->load(array_pop($issue_ids));

    if (!$issue) {
      throw new Exception('Issue could not be loaded.');
    }

    return $issue;
  }

  protected function getPublicationByRequest(Request $request = NULL): ?EpaperPublicationInterface {
    if (!$request) {
      $request = Drupal::request();
    }

    $host = $request->getHost();
    if (!$host) {
      return NULL;
    }

    $domain_parts = explode('.', $host);
    if (count($domain_parts) < 2) {
      return NULL;
    }

    $publications = Drupal::entityTypeManager()->getStorage('epaper_publication')
      ->loadByProperties([
        'slug' => array_shift($domain_parts)
      ]);

    if (count($publications) !== 1) {
      return NULL;
    }

    return array_pop($publications);
  }

  public function manifest(): JsonResponse {
    $publication = $this->getPublicationByRequest();

    if (!$publication) {
      $response = new JsonResponse();
      $response->setStatusCode(500, $this->t('No publication found'));
      return $response;
    }

    $manifest = new stdClass();
    $manifest->name = $publication->label();
    $manifest->short_name = $publication->label();
    $manifest->description = $publication->label() . ' - Epaper & more';
    $manifest->lang = Drupal::service('language_manager')->getCurrentLanguage()->getId();
    $manifest->start_url = '/app';
    $manifest->scope = '/app';
    $manifest->display = 'standalone';
    $manifest->id = $publication->id();
    // TODO: ?
    $manifest->theme_color = '#2f3d58';
    $manifest->background_color = '#2f3d58';
    $manifest->orientation = 'any';
    if (!$publication->get('logo')->isEmpty()) {
      $icon_uri = $publication->get('logo')->entity->getFileUri();

      /** @var \Drupal\image\ImageStyleInterface $imageStyle */
      $imageStyle = \Drupal::entityTypeManager()->getStorage('image_style')->load('epaper_pwa_icon_512');
      $icon_derivative_uri = $imageStyle->buildUri($icon_uri);
      if (!is_file($icon_derivative_uri)) {
        $imageStyle->createDerivative($icon_uri, $icon_derivative_uri);
      }

      $manifest->icons = [
        (object) [
          'src' => $imageStyle->buildUrl($icon_uri),
          'type' => 'image/png',
          'sizes' => '512x512',
        ]
      ];
    }

    return new JsonResponse($manifest);
  }
  public function issues(EpaperIssueInterface $epaper_issue): Response {}
  public function overview(EpaperIssueInterface $epaper_issue): Response {}
}
