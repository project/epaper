<?php

namespace Drupal\epaper\Controller;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EpaperElementEntityController extends BaseEntityController {

  public function revisionOverview(EntityInterface $epaper_element): array {
    return parent::revisionOverview($epaper_element);
  }

  public function revisionPageTitle(EntityInterface $epaper_element): string {
    return parent::revisionPageTitle($epaper_element);
  }

  public function revisionShow(EntityInterface $epaper_element): array {
    return parent::revisionShow($epaper_element);
  }

}
