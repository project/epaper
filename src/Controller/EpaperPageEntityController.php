<?php

namespace Drupal\epaper\Controller;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class EpaperPageEntityController extends BaseEntityController {

  public function revisionOverview(EntityInterface $epaper_page): array {
    return parent::revisionOverview($epaper_page);
  }

  public function revisionPageTitle(EntityInterface $epaper_page): string {
    return parent::revisionPageTitle($epaper_page);
  }

  public function revisionShow(EntityInterface $epaper_page): array {
    return parent::revisionShow($epaper_page);
  }

  public function removeUnusedFiles(): Response {
    $file_storage = \Drupal::entityTypeManager()->getStorage('file');

    $query = \Drupal::database()
      ->select('file_usage', 'fu')
      ->fields('fu', ['fid']);
    $query->addJoin('LEFT OUTER', 'epaper_page', 'ep', '[fu].id = [ep].id');
    $query
      ->condition('fu.type', 'epaper_page')
      ->condition('ep.id', NULL, 'IS NULL');
    $fids = $query->execute()->fetchAllAssoc('fid');

    foreach ($fids as $fid => $item) {
      $file_storage->load($fid)->delete();
    }

    \Drupal::messenger()->addStatus(t('Deleted %count unused Page files.', ['%count' => count($fids)]));

    $url = Url::fromRoute('view.epaper_pages.page_1');
    return new RedirectResponse($url->toString());
  }

}
