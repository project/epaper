<?php

namespace Drupal\epaper\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class BaseEntityController extends ControllerBase implements ContainerInjectionInterface {

  protected string $entityType;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected DateFormatter $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected Renderer $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  public function revisionShow(EntityInterface $entity): array {
    $view_builder = $this->entityTypeManager()->getViewBuilder($entity->getEntityTypeId());

    return $view_builder->view($entity);
  }

  public function revisionPageTitle(EntityInterface $entity): string {
    return $this->t('Revision of %title from %date', [
      '%title' => $entity->label(),
      '%date' => $this->dateFormatter->format($entity->getRevisionCreationTime()),
    ]);
  }

  public function revisionOverview(EntityInterface $entity): array {
    $account = $this->currentUser();
    /** @var ContentEntityStorageInterface $entity_storage */
    $entity_storage = $this->entityTypeManager()->getStorage($entity->getEntityTypeId());

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $entity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all " . $entity->getEntityTypeId() . " revisions") || $account->hasPermission('administer " . $entity->getEntityTypeId() . " entities')));
    $delete_permission = (($account->hasPermission("delete all " . $entity->getEntityTypeId() . " revisions") || $account->hasPermission('administer " . $entity->getEntityTypeId() . " entities')));

    $rows = [];

    $vids = $entity_storage->getQuery()
      ->accessCheck(TRUE)
      ->allRevisions()
      ->condition($entity->getEntityType()->getKey('id'), $entity->id())
      ->sort($entity->getEntityType()->getKey('revision'), 'DESC')
      ->pager(50)
      ->execute();

    $latest_revision = TRUE;

    foreach (array_keys($vids) as $vid) {
      $revision = $entity_storage->loadRevision($vid);

      if (empty($revision)) {
        continue;
      }

      $username = [
        '#theme' => 'username',
        '#account' => $revision->getRevisionUser(),
      ];

      // Use revision link to link to revisions that are not active.
      $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
      if ($vid != $entity->getRevisionId()) {
        $link = Link::fromTextAndUrl(
          $date,
          new Url('entity.' . $entity->getEntityTypeId() . '.revision', [
            $entity->getEntityTypeId() => $entity->id(),
            $entity->getEntityTypeId() . '_revision' => $vid,
          ]))->toString();
      }
      else {
        $link = $entity->toLink($date)->toString();
      }

      $row = [];
      $column = [
        'data' => [
          '#type' => 'inline_template',
          '#template' => '{{ date }} by {{ username }}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
          '#context' => [
            'date' => $link,
            'username' => $this->renderer->renderPlain($username),
            'message' => [
              '#markup' => $revision->getRevisionLogMessage(),
              '#allowed_tags' => Xss::getHtmlTagList(),
            ],
          ],
        ],
      ];
      $row[] = $column;

      if ($latest_revision) {
        $row[] = [
          'data' => [
            '#prefix' => '<em>',
            '#markup' => $this->t('Current revision'),
            '#suffix' => '</em>',
          ],
        ];
        foreach ($row as &$current) {
          $current['class'] = ['revision-current'];
        }
        $latest_revision = FALSE;
      }
      else {
        $links = [];
        if ($revert_permission) {
          $links['revert'] = [
            'title' => $this->t('Revert'),
            'url' => Url::fromRoute('entity.' . $entity->getEntityTypeId() . '.revision_revert', [
              $entity->getEntityTypeId() => $entity->id(),
              $entity->getEntityTypeId() . '_revision' => $vid,
            ]),
          ];
        }

        if ($delete_permission) {
          $links['delete'] = [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('entity.' . $entity->getEntityTypeId() . '.revision_delete', [
              $entity->getEntityTypeId() => $entity->id(),
              $entity->getEntityTypeId() . '_revision' => $vid,
            ]),
          ];
        }

        $row[] = [
          'data' => [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ];
      }

      $rows[] = $row;
    }

    $build['entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }
}
