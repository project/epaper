<?php

namespace Drupal\epaper\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides HTML routes for entities with administrative pages.
 */
class EpaperPublicationRouteProvider extends AdminHtmlRouteProvider {
  public function getRoutes(EntityTypeInterface $entity_type): RouteCollection {
    $routes = parent::getRoutes($entity_type);

    $routes->add('entity.epaper_publication.overview', new Route(
      '/app',
      [
        '_title' => 'Overview',
        '_controller' => '\Drupal\epaper\Controller\EpaperPublicationEntityController::overview',
      ],
      [
        '_permission' => 'view published issue entities',
      ]
    )); // ??
    $routes->add('entity.epaper_publication.manifest', new Route(
      '/app/manifest.json',
      [
        '_title' => 'Manifest',
        '_controller' => '\Drupal\epaper\Controller\EpaperPublicationEntityController::manifest',
      ],
      [
        '_permission' => 'view published issue entities',
      ]
    ));
    $routes->add('entity.epaper_publication.issues', new Route(
      '/app/e',
      [
        '_title' => 'Issues',
        '_controller' => '\Drupal\epaper\Controller\EpaperPublicationEntityController::issues',
      ],
      [
        '_permission' => 'view published issue entities',
      ]
    )); // View of all issues, topping with the latest
    $routes->add('entity.epaper_publication.renderedIssue', new Route(
      '/app/e/{date}',
      [
        '_title_callback' => '\Drupal\epaper\Controller\EpaperPublicationEntityController::issueTitle',
        '_controller' => '\Drupal\epaper\Controller\EpaperPublicationEntityController::issue',
      ],
      [
        '_permission' => 'view published issue entities',
      ]
    ));

    return $routes;
  }
}
