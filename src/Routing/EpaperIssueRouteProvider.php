<?php

namespace Drupal\epaper\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class EpaperIssueRouteProvider extends GenericEntityRouteProvider {

  protected string $entity_controller_class = 'Drupal\epaper\Controller\EpaperIssueEntityController';

  public function getRoutes(EntityTypeInterface $entity_type): RouteCollection {
    $routes = parent::getRoutes($entity_type);

    $routes->add('entity.epaper_issue.rendered', new Route(
      '/epaper/{epaper_issue}',
      [
        '_title' => 'Delete unused issue files',
        '_controller' => '\Drupal\epaper\Controller\EpaperIssueEntityController::removeUnusedFiles',
      ],
      [
        '_permission' => 'view published issue entities',
      ],
      [
        'parameters' => [
          'epaper_issue' => [
            'type' => 'entity:epaper_issue',
          ],
        ]
      ]
    ));

    $routes->add('entity.epaper_issue.unused_files', new Route(
      '/admin/epaper/issues/unused-files',
      [
        '_title' => 'Delete unused issue files',
        '_controller' => '\Drupal\epaper\Controller\EpaperIssueEntityController::removeUnusedFiles',
      ],
      [
        '_permission' => 'view published issue entities',
      ]
    ));

    return $routes;
  }

}
