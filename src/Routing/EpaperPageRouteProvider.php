<?php

namespace Drupal\epaper\Routing;

use Drupal;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class EpaperPageRouteProvider extends GenericEntityRouteProvider {

  protected string $entity_controller_class = 'Drupal\epaper\Controller\EpaperPageEntityController';

  public function getRoutes(EntityTypeInterface $entity_type): RouteCollection {
    $routes = parent::getRoutes($entity_type);

    $routes->add('entity.epaper_page.unused_files', new Route(
      '/admin/epaper/pages/unused-files',
      [
        '_title' => 'Delete unused page files',
        '_controller' => '\Drupal\epaper\Controller\EpaperPageEntityController::removeUnusedFiles',
      ],
      [
        '_permission' => 'view published issue entities',
      ]
    ));

    return $routes;
  }
}
