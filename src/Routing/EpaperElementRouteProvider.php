<?php

namespace Drupal\epaper\Routing;

use Drupal;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class EpaperElementRouteProvider extends GenericEntityRouteProvider {

  protected string $entity_controller_class = 'Drupal\epaper\Controller\EpaperElementEntityController';

}
