<?php

namespace Drupal\epaper\Routing;

use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Drupal\Core\Entity\Routing\EntityRouteProviderInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

abstract class GenericEntityRouteProvider extends DefaultHtmlRouteProvider implements EntityRouteProviderInterface, EntityHandlerInterface {

  protected string $entity_controller_class;

  public function getRoutes(EntityTypeInterface $entity_type): RouteCollection {
    /** @var \Symfony\Component\Routing\RouteCollection $routes */
    $routes = parent::getRoutes($entity_type);

    $routes->add('entity.' . $entity_type->id() . '.version_history', new Route(
      '/admin/epaper/' . $entity_type->id() . '/{' . $entity_type->id() . '}/revisions',
      [
        '_title' => 'Revisions',
        '_controller' => $this->entity_controller_class . '::revisionOverview',
      ],
      [
        '_permission' => $entity_type->id() . ' view revisions',
        $entity_type->id() => '\d+',
      ],
      [
        'parameters' => [
          $entity_type->id() => ['type' => 'entity:' . $entity_type->id()],
        ]
      ]
    ));

    $routes->add('entity.' . $entity_type->id() . '.revision', new Route(
      '/admin/epaper/' . $entity_type->id() . '/{' . $entity_type->id() . '}/revisions/{' . $entity_type->id() . '_revision}/view',
      [
        '_controller' => $this->entity_controller_class . '::revisionShow',
        '_title_callback' => $this->entity_controller_class . '::revisionPageTitle',
      ],
      [
        '_permission' => $entity_type->id() . ' view revisions',
        $entity_type->id() => '\d+',
        $entity_type->id() . '_revision' => '\d+',
      ],
      [
        'parameters' => [
          $entity_type->id() => ['type' => 'entity:' . $entity_type->id()],
          $entity_type->id() . '_revision' => ['type' => 'entity_revision:' . $entity_type->id()],
        ]
      ]
    ));

    $routes->add('entity.' . $entity_type->id() . '.revision_revert', new Route(
      '/admin/epaper/' . $entity_type->id() . '/{' . $entity_type->id() . '}/revisions/{' . $entity_type->id() . '_revision}/revert',
      [
        '_form' => '\Drupal\epaper\Form\RevisionRevertForm',
        '_title' => 'Revert to earlier revision',
      ],
      [
        '_permission' => $entity_type->id() . ' view revisions',
        $entity_type->id() => '\d+',
        $entity_type->id() . '_revision' => '\d+',
      ],
      [
        'parameters' => [
          $entity_type->id() => ['type' => 'entity:' . $entity_type->id()],
          $entity_type->id() . '_revision' => ['type' => 'entity_revision:' . $entity_type->id()],
        ]
      ]
    ));

    $routes->add('entity.' . $entity_type->id() . '.revision_delete', new Route(
      '/admin/epaper/' . $entity_type->id() . '/{' . $entity_type->id() . '}/revisions/{' . $entity_type->id() . '_revision}/delete',
      [
        '_form' => '\Drupal\epaper\Form\RevisionDeleteForm',
        '_title' => 'Delete earlier revision',
      ],
      [
        '_permission' => $entity_type->id() . ' view revisions',
        $entity_type->id() => '\d+',
        $entity_type->id() . '_revision' => '\d+',
      ],
      [
        'parameters' => [
          $entity_type->id() => ['type' => 'entity:' . $entity_type->id()],
          $entity_type->id() . '_revision' => ['type' => 'entity_revision:' . $entity_type->id()],
        ]
      ]
    ));

    return $routes;
  }

}
