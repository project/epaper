<?php

namespace Drupal\epaper;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of epaper variant type entities.
 *
 * @see \Drupal\epaper\Entity\EpaperVariantType
 */
class EpaperVariantTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Label');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No epaper variant types available. <a href=":link">Add epaper variant type</a>.',
      [':link' => Url::fromRoute('entity.epaper_variant_type.add_form')->toString()]
    );

    return $build;
  }

}
