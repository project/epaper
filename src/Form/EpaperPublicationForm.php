<?php

namespace Drupal\epaper\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the publication entity edit forms.
 */
class EpaperPublicationForm extends ContentEntityForm {

  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    if ($form['variants'] ?? NULL) {
      $form['variants']['#type'] = 'details';
      $form['variants']['#title'] = $form['variants']['widget']['#title'];
    }

    if ($form['default_issue_type'] ?? NULL) {
      if (empty($form['default_issue_type']['widget']['#default_value'])) {
        $form['default_issue_type']['widget']['#default_value'] = ['standard'];
      }

      if (
        count($form['default_issue_type']['widget']['#options']) ?? 0 < 2
        && $form['default_issue_type']['widget']['#default_value']
      ) {
        $form['default_issue_type'] = [
          '#type' => 'item',
          '#value' => $form['default_issue_type']['widget']['#default_value'],
        ];
      }
    }

    if ($form['default_page_type'] ?? NULL) {
      if (empty($form['default_page_type']['widget']['#default_value'])) {
        $form['default_page_type']['widget']['#default_value'] = ['default_page'];
      }

      if (
        count($form['default_page_type']['widget']['#options']) ?? 0 < 2
        && $form['default_page_type']['widget']['#default_value']
      ) {
        $form['default_page_type'] = [
          '#type' => 'item',
          '#value' => $form['default_page_type']['widget']['#default_value'],
        ];
      }
    }

    if (
      isset($form['path'])
      && $this->moduleHandler->moduleExists('token')
    ) {
      $form['path']['tree'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['epaper_publication', 'epaper_issue'],
        '#click_insert' => TRUE,
        '#dialog' => TRUE,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New publication %label has been created.', $message_arguments));
        $this->logger('epaper')->notice('Created new publication %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The publication %label has been updated.', $message_arguments));
        $this->logger('epaper')->notice('Updated publication %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.epaper_publication.canonical', ['epaper_publication' => $entity->id()]);

    return $result;
  }

}
