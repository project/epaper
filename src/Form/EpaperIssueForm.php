<?php

namespace Drupal\epaper\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Imagick;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Epaper edit forms.
 *
 * @ingroup epaper
 */
class EpaperIssueForm extends ContentEntityForm {

  protected AccountProxyInterface $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): EpaperIssueForm {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\epaper\Entity\EpaperIssueInterface $issue */
    $issue = $this->entity;

    if ($form['variant']) {
      // TODO: Filter variants by parent publication.
      if (count($form['variant']['widget']['#options']) < 2) {
        unset($form['variant']);
      }
    }

    if ($form['publication_id']) {
      $publication_id = \Drupal::request()->query->get('publication');
      if ($publication_id && empty($form['publication_id']['widget']['#default_value'])) {
        $form['publication_id'] = [
          '#type' => 'item',
          '#value' => [$publication_id],
        ];
      }
    }

    $form['issue_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Issue Date'),
      '#default_value' => date('Y-m-d', $issue->get('issue_date')?->value ?? time()),
    ];

    if (!$issue->isNew()) {
      $form['full_pdf']['pages_by_pdf'] = [
        '#type' => 'submit',
        '#submit' => [[$this, 'pagesByPdf']],
        '#value' => $this->t('Re- / Create Pages & Media by PDF'),
        '#weight' => 20,
        '#states' => [
          'visible' => [
            'input[name="files[full_pdf_0]"]' => ['filled' => TRUE],
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $issue = $this->entity;

    $issue->set('issue_date', strtotime($form_state->getValue('issue_date')));

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Epaper.', [
          '%label' => $issue->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Epaper.', [
          '%label' => $issue->label(),
        ]));
    }

    $form_state->setRedirect('entity.epaper_issue.canonical', ['epaper_issue' => $issue->id()]);
  }

  public function pagesByPdf(array $form, FormStateInterface $form_state): void {
    $issue = $this->entity;

    /** @var \Drupal\file\Plugin\Field\FieldType\FileItem $pdf_file_item */
    $pdf_file_item = $issue->get('full_pdf')->first();

    $pdf_file_path = $pdf_file_item->entity?->getFileUri();

    if (!is_file($pdf_file_path)) {
      return;
    }

    $imagick = new Imagick();
    $imagick->readImage($pdf_file_path);
    $pages_number = $imagick->getNumberImages();

    $batch = [
      'title' => t('Create pages by issue PDF'),
      'operations' => [],
      'init_message'     => t('Commencing'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message'    => t('An error occurred during processing'),
    ];
    for ($i = 1; $i <= $pages_number; $i++) {
      $batch['operations'][] = [[$issue, 'pdf2Page'], [$i]];
    }
    batch_set($batch);
  }

  public function batchPageByPdf(int $page_number, array &$context): void {
    /** @var \Drupal\epaper\Entity\EpaperIssueInterface $issue */
    $issue = $this->entity;

    $issue->pdf2Page($page_number);

    $context['message'] = t('Created page %page', ['%page' => $page_number]);
  }



}
