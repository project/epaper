<?php

namespace Drupal\epaper\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ElementTypeForm.
 */
class ElementTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $epaper_element_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $epaper_element_type->label(),
      '#description' => $this->t("Label for the Element type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $epaper_element_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\epaper\Entity\ElementType::load',
      ],
      '#disabled' => !$epaper_element_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $epaper_element_type = $this->entity;
    $status = $epaper_element_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Element type.', [
          '%label' => $epaper_element_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Element type.', [
          '%label' => $epaper_element_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($epaper_element_type->toUrl('collection'));
  }

}
