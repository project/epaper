<?php

namespace Drupal\epaper\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EpaperIssueTypeForm.
 */
class EpaperIssueTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\epaper\Entity\EpaperIssueType $epaper_issue_type */
    $epaper_issue_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $epaper_issue_type->label(),
      '#description' => $this->t("Label for the Epaper type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $epaper_issue_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\epaper\Entity\EpaperIssueType::load',
      ],
      '#disabled' => !$epaper_issue_type->isNew(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $epaper_issue_type = $this->entity;
    $status = $epaper_issue_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Epaper type.', [
          '%label' => $epaper_issue_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Epaper type.', [
          '%label' => $epaper_issue_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($epaper_issue_type->toUrl('collection'));
  }

}
