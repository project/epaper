<?php

namespace Drupal\epaper\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PageTypeForm.
 */
class EpaperPageTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $epaper_page_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $epaper_page_type->label(),
      '#description' => $this->t("Label for the Page type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $epaper_page_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\epaper\Entity\PageType::load',
      ],
      '#disabled' => !$epaper_page_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $epaper_page_type = $this->entity;
    $status = $epaper_page_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Page type.', [
          '%label' => $epaper_page_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Page type.', [
          '%label' => $epaper_page_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($epaper_page_type->toUrl('collection'));
  }

}
