<?php

namespace Drupal\epaper\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Element edit forms.
 *
 * @ingroup epaper
 */
class ElementForm extends ContentEntityForm {

  protected AccountProxyInterface $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ElementForm {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\epaper\Entity\EpaperElement $entity */
    $form = parent::buildForm($form, $form_state);

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision')) {
      $this->entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $this->entity->setRevisionCreationTime($this->time->getRequestTime());
      $this->entity->setRevisionUserId($this->account->id());
    }
    else {
      $this->entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Element.', [
          '%label' =>  $this->entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Element.', [
          '%label' =>  $this->entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.epaper_element.canonical', ['epaper_element' =>  $this->entity->id()]);
  }

}
