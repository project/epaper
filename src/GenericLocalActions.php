<?php

namespace Drupal\epaper;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class GenericLocalActions extends DeriverBase implements ContainerDeriverInterface {

  protected string $entity_type;

  use StringTranslationTrait;

  public function __construct(
    protected TranslationInterface $string_translation,
    protected EntityTypeManagerInterface $entity_type_manager
  ) {}

  public static function create(ContainerInterface $container, $base_plugin_id): GenericLocalActions {
    return new static(
      $container->get('string_translation'),
      $container->get('entity_type.manager'),
    );
  }

  public function getDerivativeDefinitions($base_plugin_definition): array {

    if (!isset($this->entity_type)) {
      return $this->derivatives;
    }

    $definition = $this->entity_type_manager->getDefinition($this->entity_type);

    $this->derivatives[$this->entity_type . ".type_add"] = [
      'title' => $this->t('Add @type type', ['@type' => $definition->getLabel()]),
      'route_name' => "entity." . $this->entity_type . "_type.add_form",
      'appears_on' => ["entity." . $this->entity_type . "_type.collection"],
    ] + $base_plugin_definition;

    $this->derivatives[$this->entity_type . ".add_page"] = [
      'title' => $this->t('Add @type', ['@type' => $definition->getLabel()]),
      'route_name' => "entity." . $this->entity_type . ".add_page",
      'appears_on' => ["entity." . $this->entity_type . ".collection"],
    ] + $base_plugin_definition;

    return $this->derivatives;
  }
}
