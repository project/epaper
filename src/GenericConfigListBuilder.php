<?php

namespace Drupal\epaper;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a listing of entities with ID, Label, Bundle columns.
 *
 * @ingroup entity_api
 */
class GenericConfigListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    if ($this->entityType->hasKey('label')) {
      $header['label'] = $this->t('Label');
    }

    return  array_merge($header, parent::buildHeader());
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    if ($this->entityType->hasKey('label')) {
      $row['label'] = new Link($entity->label() ?? '[' . $entity->id() . ']', $entity->toUrl());
    }

    return array_merge($row, parent::buildRow($entity));
  }

}
