<?php

namespace Drupal\epaper_daily_regional\EpaperImporter;

use ArrayObject;
use Drupal;
use Drupal\epaper\EpaperBatchImporterBase;
use Drupal\file\Entity\File;
use Exception;

abstract class DailyRegional extends EpaperBatchImporterBase {

  protected static string $epaperType;
  protected static string $regionTaxonomy;
  protected static string $regionFieldName;

  public static function getBatch(string $uri): array {
    $batch = parent::getBatch($uri);

    $batch['operations'] = [];
    $batch['operations']['moveArchiveToTemp'] = [static::class . '::moveArchiveToTemp',[$uri]];
    $batch['operations']['extractToTemp'] = [static::class . '::extractToTemp', []];
    $batch['operations']['analyze'] = [static::class . '::analyze', []];
    $batch['operations']['createIssues'] = [static::class . '::createIssues', []];
    $batch['operations']['createPages'] = [static::class . '::createPages', []];
    $batch['operations']['createImagesFromPDF'] = [static::class . '::createImagesFromPDF', []];
    $batch['operations']['createElements'] = [static::class . '::createElements', []];
    $batch['operations']['mergePDFs'] = [static::class . '::mergePDFs', []];
    $batch['operations']['cleanup'] = [static::class . '::cleanup', []];

    return $batch;
  }

  public static function createIssues(ArrayObject|array &$context): void {
    [$issue_index, $issue_data] = self::getFirstUnprocessed($context['results']['issues'], __METHOD__);

    if (empty($issue_data)) {
      throw new Exception('Creating issue failed - No issue data available');
    }

    $epaper_storage = Drupal::entityTypeManager()->getStorage('epaper_issue');

    // TODO: region_id is optional

    $region_tid = FALSE;
    if (!empty($issue_data['region_id'])) {
      $tids = Drupal::entityQuery('taxonomy_term')
        ->condition('vid', static::$regionTaxonomy)
        ->condition(static::$regionFieldName, $issue_data['region_id'])
        ->execute();

      if (count($tids) !== 1) {
        throw new Exception('Creating issue failed - No unique region Term ID');
      }

      $region_tid = array_pop($tids);
    }

    $criteria = [
      'type' => static::$epaperType,
      'issue_date' => $issue_data['date'],
    ];

    if ($region_tid) {
      $criteria['field_region'] = $region_tid;
    }

    $issue_ids = $epaper_storage->loadByProperties($criteria);

    $issue_updated = FALSE;
    switch (count($issue_ids)) {
      case 0:
        /** @var \Drupal\epaper\Entity\EpaperIssueInterface $issue */
        $issue = $epaper_storage->create([
          'type' => static::$epaperType,
          'issue_date' => $issue_data['date'],
          'pages' => count($issue_data['pages']),
        ]);

        if ($region_tid) {
          $issue->set('field_region', $region_tid);
        }

        $issue->setNewRevision();
        $issue->setRevisionLogMessage('Creating by batch import');

        $issue->save();
        break;

      case 1:
        $issue_updated = TRUE;

        /** @var \Drupal\epaper\Entity\EpaperIssueInterface $issue */
        $issue = array_pop($issue_ids);

        $issue->setNewRevision();
        $issue->setRevisionLogMessage('Updating by batch import');
        break;

      default:
        throw new Exception('Creating issue failed - Conflicting issues found');
    }

    $issue_data['id'] = $issue->id();
    $issue_data['label'] = $issue->label();

    if (empty($issue_data['id'])) {
      throw new Exception('Creating Issue failed - No issue ID set');
    }

    $issue_data[__METHOD__ . '_processed'] = TRUE;

    $context['results']['issues'][$issue_index] = $issue_data;

    $context['finished'] = self::getProcessedCount($context['results']['issues'], __METHOD__) / count($context['results']['issues']);

    $context['message'] = t(($issue_updated ? 'Updating' : 'Created') . ' issue %issue', ['%issue' => $issue_data['label']]);
  }

  public static function createPages(ArrayObject|array &$context): void {
    [$issue_index, $issue_data] = self::getFirstUnprocessed($context['results']['issues'], __METHOD__);

    /** @var \Drupal\epaper\Entity\EpaperIssueInterface $issue */
    $issue = Drupal::entityTypeManager()->getStorage('epaper_issue')->load($issue_data['id']);

    $page_storage = Drupal::entityTypeManager()->getStorage('epaper_page');

    if (empty($issue)) {
      throw new Exception('Creating pages failed - No issue loaded');
    }

    $file_system = self::getFileSystem();

    [$page_index, $page_data] = self::getFirstUnprocessed($issue_data['pages'], __METHOD__);

    if (empty($page_data['pdf']) || !is_file($page_data['pdf'])) {
      throw new Exception('Creating pages failed - Page ' . $page_data['number'] . ' PDF missing at ' . $page_data['pdf']);
    }

    /** @var \Drupal\epaper\Entity\DefaultEpaperPage $page */
    $pages = $page_storage->loadByProperties([
      'epaper_id' => $issue->id(),
      'page_number' => $page_data['number'],
    ]);

    $page_existing = FALSE;

    if ($pages) {
      $page_existing = TRUE;

      $page = array_pop($pages);
      $page->set('name', $page_data['title']);
      $page->set('cover_page', $page_data['cover_page']);
      $page->set('double_page', $page_data['double_page']);
    }
    else {
      /** @var \Drupal\epaper\Entity\DefaultEpaperPage $page */
      $page = $page_storage->create([
        'epaper_id' => $issue->id(),
        'page_number' => $page_data['number'],
        'name' => $page_data['title'],
        'double_page' => $page_data['double_page'],
        'type' => 'default_page',
        'cover_page' => $page_data['cover_page'],
        'width' =>  $page_data['width'],
        'height' => $page_data['height'],
      ]);
    }

    $page_pdf_field = $page->get('field_page_pdf');
    $image_field = $page->get('image');
    if (
      $page_pdf_field->isEmpty()
      || md5_file($page_pdf_field->entity?->getFileUri()) !== md5_file($page_data['pdf'])
    ) {
      $context['results']['issues'][$issue_index]['pages'][$page_index]['pdf_altered'] = TRUE;

      if (!$image_field->isEmpty()) {
        $existing_image_path = $image_field->entity?->getFileUri();
        $image_field->entity?->delete();
        if (is_file($existing_image_path)) {
          $file_system->delete($existing_image_path);
        }
        $image_field->removeItem(0);
      }

      if (!$page_pdf_field->isEmpty()) {
        $page_pdf_field->entity->delete();
        $file_system->delete($page_pdf_field->entity?->getFileUri());
        $page_pdf_field->removeItem(0);
      }

      if ($file_path = $file_system->copy($page_data['pdf'], $issue->getDirectory() . 'DailyRegional.php/' . $file_system->basename($page_data['pdf'], $file_system::EXISTS_REPLACE))) {
        $file = File::create([
          'uri' => $file_path,
          'status' => 1,
        ]);
        $file->save();
        $page->get('field_page_pdf')->appendItem($file->id());
      }
      else {
        throw new Exception('Creating pages failed - Page ' . $page_data['number'] . ' PDF ' . $page_data['pdf'] . ' could not be moved');
      }
    }

    if (!empty($page_data['image'])) {
      if (!$image_field->isEmpty()) {
        $existing_image_path = $image_field->entity?->getFileUri();
        $image_field->entity?->delete();
        if (is_file($existing_image_path)) {
          $file_system->delete($existing_image_path);
        }
        $image_field->removeItem(0);
      }

      if ($file_path = $file_system->copy($page_data['image'], $issue->getDirectory() . 'DailyRegional.php/' . $file_system->basename($page_data['image'], $file_system::EXISTS_REPLACE))) {
        $file = File::create([
          'uri' => $file_path,
          'status' => 1,
        ]);
        $file->save();
        $image_field->appendItem($file->id());
      }
    }

    $page->setNewRevision();
    $page->setRevisionLogMessage('Creating/updating by batch import');

    $page->save();

    $context['results']['issues'][$issue_index]['pages'][$page_index][__METHOD__ . '_processed'] = TRUE;
    if (self::getProcessedCount($context['results']['issues'][$issue_index]['pages'], __METHOD__) == count($context['results']['issues'][$issue_index]['pages'])) {
      $context['results']['issues'][$issue_index][__METHOD__ . '_processed'] = TRUE;
    }

    $context['results']['issues'][$issue_index]['pages'][$page_index]['id'] = $page->id();
    $context['results']['issues'][$issue_index]['pages'][$page_index]['label'] = $page->label();

    $context['results']['issues'][$issue_index]['pages'][$page_index]['create_image_from_pdf'] = $page->get('image')->isEmpty();

    $context['finished'] = self::getProcessedCount($context['results']['issues'], __METHOD__) / count($context['results']['issues']);

    $context['message'] = t(($page_existing ? 'Updated' : 'Created new') . ' page %page', ['%page' => $page->label()]);
  }

  public static function createImagesFromPDF(ArrayObject|array &$context): void {
    [$issue_index, $issue_data] = self::getFirstUnprocessed($context['results']['issues'], __METHOD__);

    if (empty($issue_data)) {
      throw new Exception('Creating page images failed - No issue loaded');
    }

    [$page_index, $page_data] = self::getFirstUnprocessed($issue_data['pages'], __METHOD__);

    $page_storage = Drupal::entityTypeManager()->getStorage('epaper_page');

    if ($page_data['create_image_from_pdf']) {
      /** @var \Drupal\epaper\Entity\DefaultEpaperPage $page */
      $page = $page_storage->load($page_data['id']);

      if (!$page) {
        throw new Exception('Creating PDFs failed - Page ' . $page_data['number'] . ' could not be loaded');
      }

      $page->pdf2Image();
    }

    $context['results']['issues'][$issue_index]['pages'][$page_index][__METHOD__ . '_processed'] = TRUE;
    if (self::getProcessedCount($context['results']['issues'][$issue_index]['pages'], __METHOD__) == count($context['results']['issues'][$issue_index]['pages'])) {
      $context['results']['issues'][$issue_index][__METHOD__ . '_processed'] = TRUE;
    }

    $context['finished'] = self::getProcessedCount($context['results']['issues'], __METHOD__) / count($context['results']['issues']);

    $context['message'] = t(($page_data['create_image_from_pdf'] ? 'Created image from PDF for' : 'Skip image creation on unaltered PDF for') . ' page %page', ['%page' => $page_data['label']]);
  }

  public static function mergePDFs(ArrayObject|array &$context): void {
    [$issue_index, $issue_data] = self::getFirstUnprocessed($context['results']['issues'], __METHOD__);

    if (empty($issue_data)) {
      throw new Exception('Creating page images failed - No issue loaded');
    }

    /** @var \Drupal\epaper_daily_regional\Entity\DailyRegionalEpaperBaseIssue $issue */
    $issue = Drupal::entityTypeManager()->getStorage('epaper_issue')->load($issue_data['id']);

    $recreate_pdf = FALSE;
    foreach ($issue_data['pages'] as $page_data) {
      if ($page_data['pdf_altered'] ?? FALSE) {
        $recreate_pdf = TRUE;
        break;
      }
    }

    if ($issue->field_full_pdf->isEmpty()) {
      $recreate_pdf = TRUE;
    }

    if (!$recreate_pdf) {
      $context['message'] = t('PDF field set and page PDFs unaltered. Skipping recreation.');
    }
    else {
      $context['message'] = t('PDF field empty or page PDFs altered. Recreating full PDF.');
      $issue->mergeFullEditionPdf();
    }

    $context['results']['issues'][$issue_index][__METHOD__ . '_processed'] = TRUE;
    $context['finished'] = self::getProcessedCount($context['results']['issues'], __METHOD__) / count($context['results']['issues']);
  }
}
