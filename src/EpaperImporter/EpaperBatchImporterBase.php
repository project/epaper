<?php

namespace Drupal\epaper;

use ArrayObject;
use Drupal;
use Exception;
use ZipArchive;

abstract class EpaperBatchImporterBase {

  protected static function getTempDirectory(ArrayObject|array $context): string {
    if (empty($context['results']['temp_directory'])) {
      throw new Exception('Analyzing archive failed - No directory set');
    }

    if (!is_dir($context['results']['temp_directory'])) {
      throw new Exception('Analyzing archive ' . $context['results']['temp_directory'] . ' failed - Directory does not exist');
    }

    return $context['results']['temp_directory'];
  }

  protected static function getFileSystem(): Drupal\Core\File\FileSystemInterface {
    return Drupal::service('file_system');
  }

  protected static function getFirstUnprocessed(ArrayObject|array $source, string $identifier): array {
    $index = $element = NULL;
    foreach ($source as $index => $element) {
      if ($element[$identifier . '_processed'] ?? FALSE) {
        $index = $element = NULL;
        continue;
      }
      break;
    }

    return [$index, $element];
  }

  protected static function getProcessedCount(ArrayObject|array $source, string $identifier): int {
    $processed = 0;
    foreach ($source as $element) {
      if ($element[$identifier . '_processed'] ?? FALSE) {
        $processed++;
      }
    }

    return $processed;
  }

  public static function getBatch(string $uri): array {
    $batch = array(
      'title' => t('Importing epaper structure...'),
      'operations' => [],
      'init_message'     => t('Commencing'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message'    => t('An error occurred during processing'),
      // todo
      'finished' => '\Drupal\batch_example\DeleteNode::ExampleFinishedCallback',
    );

    $batch['operations'][] = [static::class . '::moveArchiveToTemp',[$uri]];
    $batch['operations'][] = [static::class . '::extractToTemp', []];
    $batch['operations'][] = [static::class . '::analyze', []];
    $batch['operations'][] = [static::class . '::cleanup', []];

    return $batch;
  }

  public static function moveArchiveToTemp(string $uri, &$context): void {
    $file_system = self::getFileSystem();

    // @todo MOVE by settings?
    $context['results']['temp_name'] = $file_system->tempnam($file_system->getTempDirectory(), 'epaper_' . $file_system->basename($uri) . '_');
    $context['results']['temp_zip_file'] = $file_system->copy($uri, $context['results']['temp_name'], $file_system::EXISTS_REPLACE);

    $context['message'] = t('Moved %a to %b temporarily', ['%a' => $uri, '%b' => $context['results']['temp_zip_file']]);
  }

  public static function extractToTemp(ArrayObject|array &$context): void {
    if (empty($context['results']['temp_zip_file'])) {
      throw new Exception('Extraction failed - No filename set');
    }

    if (!is_file($context['results']['temp_zip_file'])) {
      throw new Exception('Extraction ' . $context['results']['temp_zip_file'] . '  failed - File does not exist');
    }

    $temp_file = $context['results']['temp_zip_file'];

    $file_system = self::getFileSystem();

    $zip = new ZipArchive;
    $res = $zip->open($temp_file);
    if ($res !== TRUE) {
      throw new Exception('Extraction ' . $temp_file . ' failed - Could not open');
    }

    $temp_directory = $file_system->getTempDirectory() . 'EpaperBatchImporterBase.php/' . $file_system->basename($temp_file) . '_extracted';
    if ($zip->extractTo($temp_directory) !== TRUE) {
      throw new Exception('Extraction ' . $temp_file . ' failed - Could not extract');
    }
    $zip->close();

    $context['results']['temp_directory'] = $temp_directory;

    if (empty($context['results']['temp_directory'])) {
      throw new Exception('Analyzing archive failed - No directory set');
    }

    if (!is_dir($context['results']['temp_directory'])) {
      throw new Exception('Analyzing archive ' . $context['results']['temp_directory'] . ' failed - Directory does not exist');
    }

    $context['message'] = t('Extracted %a to %b', ['%a' => $temp_file, '%b' => $temp_directory]);
  }

  public static function cleanup(ArrayObject|array &$context): void {
    $file_system = self::getFileSystem();

    $file_system->delete($context['results']['temp_zip_file']);
    $file_system->deleteRecursive($context['results']['temp_directory']);

    $context['message'] = t('Deleted %temp_file temporary file and %temp_dir extracted temporary directory', [
      '%temp_file' => $context['results']['temp_zip_file'],
      '%temp_dir' => $context['results']['temp_directory'],
    ]);
  }

}
