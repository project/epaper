<?php

namespace Drupal\epaper\Plugin\Derivative;

use Drupal\epaper\GenericLocalActions;
use Drupal\epaper\Plugin\Menu\LocalAction\EpaperPublicationParameter;

class EpaperPublicationLocalActions extends GenericLocalActions {

  protected string $entity_type = 'epaper_publication';

  public function getDerivativeDefinitions($base_plugin_definition): array {

    parent::getDerivativeDefinitions($base_plugin_definition);

    $definition = $this->entity_type_manager->getDefinition($this->entity_type);

    $this->derivatives[$this->entity_type .".add_issue"] = [
      'title' => $this->t('Add issue'),
      'route_name' => "entity.epaper_issue.add_form",
      'class' => EpaperPublicationParameter::class,
      'appears_on' => ["entity.epaper_publication.canonical"],
    ] + $base_plugin_definition;

    $this->derivatives[$this->entity_type . ".add_publication_view"] = [
      'title' => $this->t('Add @type', ['@type' => $definition->getLabel()]),
      'route_name' => "entity." . $this->entity_type . ".add_page",
      'appears_on' => ["view.epaper_publications.page_1"],
    ] + $base_plugin_definition;

    return $this->derivatives;
  }

}
