<?php

namespace Drupal\epaper\Plugin\Derivative;

use Drupal\epaper\GenericLocalTasks;

class EpaperIssueLocalTasks extends GenericLocalTasks {

  protected string $entity_type = 'epaper_issue';

  public function getDerivativeDefinitions($base_plugin_definition): array {

    parent::getDerivativeDefinitions($base_plugin_definition);

    return $this->derivatives;
  }

}
