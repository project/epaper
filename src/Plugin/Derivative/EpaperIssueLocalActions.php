<?php

namespace Drupal\epaper\Plugin\Derivative;

use Drupal\epaper\GenericLocalActions;
use Drupal\epaper\Plugin\Menu\LocalAction\EpaperIssueParameter;

class EpaperIssueLocalActions extends GenericLocalActions {

  protected string $entity_type = 'epaper_issue';

  public function getDerivativeDefinitions($base_plugin_definition): array {

    parent::getDerivativeDefinitions($base_plugin_definition);

    $definition = $this->entity_type_manager->getDefinition($this->entity_type);

    $this->derivatives[$this->entity_type .".add_new_page"] = [
      'title' => $this->t('Add page'),
      'route_name' => "entity.epaper_page.add_form",
      'class' => EpaperIssueParameter::class,
      'appears_on' => ["entity.epaper_issue.canonical"],
    ] + $base_plugin_definition;

    $this->derivatives[$this->entity_type . ".add_issue_view"] = [
      'title' => $this->t('Add @type', ['@type' => $definition->getLabel()]),
      'route_name' => "entity." . $this->entity_type . ".add_page",
      'appears_on' => ["view.epaper_issues.page_1", "epaper.admin_epaper"],
    ] + $base_plugin_definition;

    $this->derivatives[$this->entity_type . ".unused_files"] = [
      'title' => $this->t('Remove unused files'),
      'route_name' => "entity.epaper_issue.unused_files",
      'appears_on' => ["view.epaper_issues.page_1", "epaper.admin_epaper"],
    ] + $base_plugin_definition;

    return $this->derivatives;
  }

}
