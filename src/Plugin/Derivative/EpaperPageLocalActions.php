<?php

namespace Drupal\epaper\Plugin\Derivative;

use Drupal\epaper\GenericLocalActions;

class EpaperPageLocalActions extends GenericLocalActions {

  protected string $entity_type = 'epaper_page';

  public function getDerivativeDefinitions($base_plugin_definition): array {

    parent::getDerivativeDefinitions($base_plugin_definition);

    $definition = $this->entity_type_manager->getDefinition($this->entity_type);

    $this->derivatives[$this->entity_type . ".add_page_view"] = [
      'title' => $this->t('Add @type', ['@type' => $definition->getLabel()]),
      'route_name' => "entity." . $this->entity_type . ".add_page",
      'appears_on' => ["view.epaper_pages.page_1"],
    ] + $base_plugin_definition;

    $this->derivatives[$this->entity_type . ".unused_files"] = [
      'title' => $this->t('Remove unused files'),
      'route_name' => "entity.epaper_page.unused_files",
      'appears_on' => ["view.epaper_pages.page_1"],
    ] + $base_plugin_definition;

    return $this->derivatives;
  }

}
