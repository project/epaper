<?php

namespace Drupal\epaper\Plugin\Derivative;

use Drupal\epaper\GenericLocalTasks;

class EpaperElementLocalTasks extends GenericLocalTasks {

  protected string $entity_type = 'epaper_element';

  public function getDerivativeDefinitions($base_plugin_definition): array {

    parent::getDerivativeDefinitions($base_plugin_definition);

    return $this->derivatives;
  }

}
