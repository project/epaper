<?php

namespace Drupal\epaper\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Defines the 'epaper_bounds' field widget.
 *
 * @FieldWidget(
 *   id = "epaper_bounds",
 *   label = @Translation("Boundaries"),
 *   field_types = {"epaper_bounds"},
 * )
 */
class BoundariesWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {

    /** @var \Drupal\epaper\Plugin\Field\FieldType\Boundaries $item */
    $item = $items[$delta];

    /** @var \Drupal\epaper\Entity\EpaperElement $parent_element */
    $parent_element = $item->getParent()->getParent()->getEntity();

    /** @var \Drupal\epaper\Entity\EpaperPage $current_page */
    $current_page = NULL;
    if ($item->page_id ?? FALSE) {
      $current_page = \Drupal::entityTypeManager()->getStorage('epaper_page')->load($item->page_id);
    }

    $element['page_id'] = [
      '#type' => 'select',
      '#title' => 'Page',
      '#options' => [],
      '#default_value' => $item->page_id ?? '',
      '#empty_value' => '',
      '#ajax' => [
        'callback' => [$this, 'updatePageCrop']
      ],
    ];
    $page_id_state = [
      'visible' => [
        ':input[id="edit-bounds-' . $delta . '-page-id"]' => [
          '!value' => ''
        ],
      ],
    ];

    foreach ($parent_element->getEpaper()->getPages() as $page) {
      $element['page_id']['#options'][$page->id()] = $page->getNumber() . ' - ' . $page->getName();
    }

    $element['page_crop'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'epaper-boundaries-widget-page-image',
          'epaper-boundaries-widget-page-image-' . $delta,
        ],
      ],
      'image' => [
        '#theme' => 'image_style',
        '#style_name' => 'wide',
        '#uri' => $current_page?->get('image')->entity->getFileUri() ?? '',
        '#alt' => $current_page?->getNumber() . ' - ' . $current_page?->getName() ?? '',
      ],
      '#attached' => [
        'library' => [
          'epaper/boundaries-widget',
        ]
      ],
      '#states' => $page_id_state,
    ];

    $element['top'] = [
      '#title' => $this->t('Top offset'),
      '#type' => 'number',
      '#default_value' => $item->top ?? NULL,
      '#min' => 0,
      '#step' => 0.01,
      '#required' => TRUE,
      '#wrapper_attributes' => [
        'class' => [
          'epaper-boundaries-widget-bound-value',
        ],
      ],
      '#states' => $page_id_state,
    ];

    $element['left'] = [
      '#title' => $this->t('Left offset'),
      '#type' => 'number',
      '#default_value' => $item->left ?? NULL,
      '#min' => 0,
      '#step' => 0.01,
      '#required' => TRUE,
      '#wrapper_attributes' => [
        'class' => [
          'epaper-boundaries-widget-bound-value',
        ],
      ],
      '#states' => $page_id_state,
    ];

    $element['width'] = [
      '#title' => $this->t('Width'),
      '#type' => 'number',
      '#default_value' => $item->width ?? NULL,
      '#min' => 0,
      '#step' => 0.01,
      '#required' => TRUE,
      '#wrapper_attributes' => [
        'class' => [
          'epaper-boundaries-widget-bound-value',
        ],
      ],
      '#states' => $page_id_state,
    ];

    $element['height'] = [
      '#title' => $this->t('Height'),
      '#type' => 'number',
      '#default_value' => $item->height ?? NULL,
      '#min' => 0,
      '#step' => 0.01,
      '#required' => TRUE,
      '#wrapper_attributes' => [
        'class' => [
          'epaper-boundaries-widget-bound-value',
        ],
      ],
      '#states' => $page_id_state,
    ];

    return $element;
  }

  function updatePageCrop(array $form, FormStateInterface $form_state): Response|array {
    $trigger = $form_state->getTriggeringElement();
    if (!$trigger) {
      return [];
    }

    if (empty($trigger['#value'])) {
      return [];
    }

    $element = NestedArray::getValue($form, array_slice($trigger['#array_parents'], 0, -1));
    if (empty($element['page_crop'])) {
      return [];
    }

    $current_page = \Drupal::entityTypeManager()->getStorage('epaper_page')->load($trigger['#value']);

    $page_crop = $element['page_crop'];
    $page_crop['image']['#uri'] = $current_page?->get('image')->entity->getFileUri() ?? '';
    $page_crop['#alt'] = $current_page?->getNumber() . ' - ' . $current_page->getName() ?? '';

    $delta = array_slice($trigger['#array_parents'], -2, 1)[0];
    $class_name = 'epaper-boundaries-widget-page-image-' . $delta;

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('.' . $class_name, $page_crop));

    return $response;
  }

}
