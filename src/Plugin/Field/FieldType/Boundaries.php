<?php

namespace Drupal\epaper\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'coordinates' entity field type.
 *
 * @FieldType(
 *   id = "epaper_bounds",
 *   label = @Translation("Boundaries"),
 *   description = @Translation("A field containing a plain string value."),
 *   category = @Translation("Text"),
 *   default_widget = "string_textfield",
 *   default_formatter = "string"
 * )
 */
class Boundaries extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'page_id' => [
          'type' => 'int',
        ],
        'top' => [
          'type' => 'float',
        ],
        'left' => [
          'type' => 'float',
        ],
        'width' => [
          'type' => 'float',
        ],
        'height' => [
          'type' => 'float',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $values['top'] = 10;
    $values['left'] = 10;
    $values['width'] = 50;
    $values['height'] = 50;
    $values['page_id'] = NULL;
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['top'] = DataDefinition::create('float')
      ->setLabel(t('Top'));

    $properties['left'] = DataDefinition::create('float')
      ->setLabel(t('Left'));

    $properties['width'] = DataDefinition::create('float')
      ->setLabel(t('Width'));

    $properties['height'] = DataDefinition::create('float')
      ->setLabel(t('Height'));

    $properties['page_id'] = DataDefinition::create('integer')
      ->setLabel(t('Page ID'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    if (
      ($this->get('top')->getValue() === NULL || $this->get('top')->getValue() === '')
      || ($this->get('left')->getValue() === NULL || $this->get('left')->getValue() === '')
      || ($this->get('width')->getValue() === NULL || $this->get('width')->getValue() === '')
      || ($this->get('height')->getValue() === NULL || $this->get('height')->getValue() === '')
      || ($this->get('page_id')->getValue() === NULL || $this->get('page_id')->getValue() === '')
    ) {
      return TRUE;
    }
    return FALSE;
  }

}
