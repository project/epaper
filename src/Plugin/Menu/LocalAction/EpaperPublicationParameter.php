<?php

namespace Drupal\epaper\Plugin\Menu\LocalAction;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;

class EpaperPublicationParameter extends LocalActionDefault {

  public function getRouteParameters(RouteMatchInterface $route_match): array {

    /** @var \Drupal\epaper\Entity\EpaperPublicationInterface $publication */
    $publication = $route_match->getParameter('epaper_publication');

    if (!$publication->get('default_issue_type')->isEmpty()) {
      $default_issue_type = $publication->get('default_issue_type')->first()->getValue()['target_id'];
    }

    if (empty($default_issue_type)) {
      $default_issue_type = 'standard';
    }

    return [
      'epaper_issue_type' => $default_issue_type,
      'publication' => $route_match->getRawParameter('epaper_publication')
    ];
  }

}
