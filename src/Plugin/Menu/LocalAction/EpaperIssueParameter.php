<?php

namespace Drupal\epaper\Plugin\Menu\LocalAction;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;

class EpaperIssueParameter extends LocalActionDefault {

  public function getRouteParameters(RouteMatchInterface $route_match): array {

    /** @var \Drupal\epaper\Entity\EpaperIssueInterface $publication */
    $issue = $route_match->getParameter('epaper_issue');

    if (!$issue->get('publication_id')?->entity?->get('default_page_type')->isEmpty()) {
      $default_page_type = $issue->get('publication_id')?->entity?->get('default_page_type')->first()->getValue()['target_id'];
    }

    if (empty($default_page_type)) {
      $default_page_type = 'default_page';
    }

    return [
      'epaper_page_type' => $default_page_type,
      'epaper_issue' => $route_match->getRawParameter('epaper_issue')
    ];
  }

}
