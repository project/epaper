<?php

namespace Drupal\epaper\Plugin\views\field;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("epaper_issue_rendered_link")
 */
class EpaperIssueRenderedLink extends FieldPluginBase  {

  public function query() {
    // Leave empty to avoid a query on this field.
  }

  public function render(ResultRow $values): string {
    /** @var \Drupal\epaper\Entity\EpaperIssueInterface $issue */
    $issue = $this->getEntity($values);

    return $issue->getRenderedUrl();
  }

}
