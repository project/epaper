<?php

namespace Drupal\epaper;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class GenericLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  protected string $entity_type;

  use StringTranslationTrait;

  public function __construct(
    protected TranslationInterface $string_translation,
    protected EntityTypeManagerInterface $entity_type_manager
  ) {}

  public static function create(ContainerInterface $container, $base_plugin_id): GenericLocalTasks {
    return new static(
      $container->get('string_translation'),
      $container->get('entity_type.manager'),
    );
  }

  public function getDerivativeDefinitions($base_plugin_definition): array {

    if (!isset($this->entity_type)) {
      return $this->derivatives;
    }

    $definition = $this->entity_type_manager->getDefinition($this->entity_type);

    $this->derivatives["entity." . $this->entity_type . ".view"] = [
      'route_name' => "entity." . $this->entity_type . ".canonical",
      'title' => $this->t('View'),
      'base_route' => "entity." . $this->entity_type . ".canonical",
    ] + $base_plugin_definition;

    $this->derivatives["entity." . $this->entity_type . ".edit_form"] = [
      'route_name' => "entity." . $this->entity_type . ".edit_form",
      'title' => $this->t('Edit'),
      'base_route' => "entity." . $this->entity_type . ".canonical",
    ] + $base_plugin_definition;

    $this->derivatives["entity." . $this->entity_type . ".delete_form"] = [
      'route_name' => "entity." . $this->entity_type . ".delete_form",
      'title' => $this->t('Delete'),
      'base_route' => "entity." . $this->entity_type . ".canonical",
      'weigh' => 10,
    ] + $base_plugin_definition;

    if ($definition->isRevisionable()) {
      $this->derivatives["entity." . $this->entity_type . ".version_history"] = [
        'route_name' => "entity." . $this->entity_type . ".version_history",
        'title' => $this->t('Revisions'),
        'base_route' => "entity." . $this->entity_type . ".canonical",
      ] + $base_plugin_definition;
    }

    $this->derivatives["entity." . $this->entity_type . "_type.collection"] = [
      'route_name' => "entity." . $this->entity_type . "_type.collection",
      'title' => $this->t('List'),
      'base_route' => "entity." . $this->entity_type . "_type.collection",
    ] + $base_plugin_definition;

    $this->derivatives["entity." . $this->entity_type . "_type.edit_form"] = [
      'route_name' => "entity." . $this->entity_type . "_type.edit_form",
      'title' => $this->t('Edit'),
      'base_route' => "entity." . $this->entity_type . "_type.edit_form",
    ] + $base_plugin_definition;

    return $this->derivatives;
  }
}
