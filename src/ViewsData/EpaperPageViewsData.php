<?php

namespace Drupal\epaper\ViewsData;

use Drupal\views\EntityViewsData;

class EpaperPageViewsData extends EntityViewsData {

  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['epaper_page']['bulk_form'] = [
      'title' => $this->t('Epaper pages bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple pages.'),
      'field' => [
        'title' => $this->t('Epaper pages bulk form - title'),
        'label' => $this->t('Epaper pages bulk form - label'),
        'id' => 'bulk_form',
      ],
    ];

    if (isset($data['epaper_page']['issue_id']['relationship'])) {
      $data['epaper_page']['issue_id']['relationship']['base'] = 'epaper_issue';
    }

    return $data;
  }

}
