<?php

namespace Drupal\epaper\ViewsData;

use Drupal\views\EntityViewsData;

class EpaperIssueViewsData extends EntityViewsData {

  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['epaper_issue']['rendered_link'] = [
      'title' => t('Issue rendered link'),
      'help' => t('Link to issue rendered in epaper theme.'),
      'field' => [
        'id' => 'epaper_issue_rendered_link',
        'title' => t('Issue rendered link'),
        'help' => t('Link to issue rendered in epaper theme.'),
      ],
    ];

    $data['epaper_issue']['epaper_pages'] = [
      'title' => t('Pages'),
      'help' => t('Pages assigned to this issue'),
      'relationship' => [
        'base' => 'epaper_page',
        'base field' => 'issue_id',
        'relationship field' => 'id',
        'id' => 'standard',
        'label' => t('Issue assigned pages'),
      ],
    ];

    $data['epaper_issue']['first_page'] = [
      'title' => t('First page'),
      'help' => t('Front page of this issue.'),
      'group' => t('Issue'),
      'entity_type' => 'epaper_page',
      'relationship' => [
        'base' => 'epaper_page',
        'base field' => 'issue_id',
        'relationship field' => 'id',
        'label' => t('First page'),
        'help' => t('Front page of this issue.'),
        'id' => 'standard',
        'extra' => [['field' => 'page_number', 'value' => '1']],
      ],
    ];

    $data['epaper_issue']['bulk_form'] = [
      'title' => $this->t('Epaper issues bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple issues.'),
      'field' => [
        'id' => 'bulk_form',
      ],
    ];

    return $data;
  }

}
