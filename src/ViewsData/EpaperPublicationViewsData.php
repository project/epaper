<?php

namespace Drupal\epaper\ViewsData;

use Drupal\views\EntityViewsData;

class EpaperPublicationViewsData extends EntityViewsData {

  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['epaper_publication']['bulk_form'] = [
      'title' => $this->t('Epaper publication bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple publication.'),
      'field' => [
        'id' => 'bulk_form',
      ],
    ];

    return $data;
  }

}
