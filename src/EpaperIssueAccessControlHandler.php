<?php

namespace Drupal\epaper;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Epaper entity.
 *
 * @see \Drupal\epaper\Entity\Epaper.
 */
class EpaperIssueAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\epaper\Entity\EpaperIssueInterface $entity */

    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished issue entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published issue entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit issue entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete issue entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add issue entities');
  }

}
