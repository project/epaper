<?php

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\user\Entity\User;

/**
 * Implements hook_token_info().
 */
function epaper_token_info(): array {
  return [
    'types' => [
      'epaper_publication' => [
        'name' => t('Epaper Publication'),
        'description' => t('Tokens related to individual publications.'),
        'needs-data' => 'epaper_publication',
      ],
      'epaper_issue' => [
        'name' => t('Epaper Issue'),
        'description' => t('Tokens related to individual issues.'),
        'needs-data' => 'epaper_issue',
      ],
      'epaper_page' => [
        'name' => t('Epaper Page'),
        'description' => t('Tokens related to individual pages.'),
        'needs-data' => 'epaper_page',
      ],
    ],
    'tokens' => [
      'epaper_publication' => [
        'slug' => [
          'name' => t("Slug"),
        ],
        'label' => [
          'name' => t("Label"),
        ],
      ],
      'epaper_issue' => [
        'name' => [
          'name' => t("Name"),
        ],
        'label' => [
          'name' => t("Label"),
          'description' => t("Name and publication combined"),
        ],
        'date' => [
          'name' => t("Date"),
          'type' => 'date',
        ],
        'publication' => [
          'name' => t("Publication"),
          'type' => 'epaper_publication',
        ],
        'directory' => [
          'name' => t("Directory"),
        ],
      ],
      'epaper_page' => [
        'issue' => [
          'name' => t("Issue"),
          'type' => 'epaper_issue',
        ],
        'publication' => [
          'name' => t("Publication"),
          'type' => 'epaper_publication',
        ],
        'number' => [
          'name' => t("Number"),
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function epaper_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $token_service = Drupal::token();
  $replacements = [];

  if (empty($data[$type])) {
    return $replacements;
  }

  if ($type == 'epaper_publication' && !empty($data['epaper_publication'])) {
    /** @var \Drupal\epaper\Entity\EpaperPublicationInterface $publication */
    $publication = $data['epaper_publication'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'label':
          $replacements[$original] = $publication->label();
          break;

        case 'slug':
          $replacements[$original] = $publication->get('slug')
            ->getValue()['value'];
          break;
      }
    }
  }

  if ($type == 'epaper_issue' && !empty($data['epaper_issue'])) {
    /** @var \Drupal\epaper\Entity\EpaperIssueInterface $issue */
    $issue = $data['epaper_issue'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'name':
          $replacements[$original] = $issue->get('name')['value'];
          break;

        case 'label':
          $replacements[$original] = $issue->label();
          break;

        case 'directory':
          $replacements[$original] = $issue->getDirectory();
          break;
      }
    }

    if ($publication_tokens = $token_service->findWithPrefix($tokens, 'publication')) {
      $replacements += $token_service->generate('epaper_publication', $publication_tokens, ['epaper_publication' => $issue->getPublication()], $options, $bubbleable_metadata);
    }

    if ($date_tokens = $token_service->findWithPrefix($tokens, 'date')) {
      $replacements += $token_service->generate('date', $date_tokens, ['date' => $issue->get('issue_date')->value], $options, $bubbleable_metadata);
    }
  }

  if ($type == 'epaper_page' && !empty($data['epaper_page'])) {
    /** @var \Drupal\epaper\Entity\EpaperPageInterface $page */
    $page = $data['epaper_page'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'number':
          $replacements[$original] = $page->getNumber();
          break;
      }

      if ($issue_tokens = $token_service->findWithPrefix($tokens, 'issue')) {
        $replacements += $token_service->generate('epaper_issue', $issue_tokens, ['epaper_issue' => $page->getIssue()], $options, $bubbleable_metadata);
      }
    }

    if ($publication_tokens = $token_service->findWithPrefix($tokens, 'publication')) {
      $replacements += $token_service->generate('epaper_publication', $publication_tokens, ['epaper_publication' => $page->getIssue()->getPublication()], $options, $bubbleable_metadata);
    }
  }

  return $replacements;
}
